"""extract.py -- Extract data from milidrop's text files
(c) Guilhem Doulcier - 2016 - GPLv3+
"""
import logging
import os
import glob
import pandas
import datetime
import numpy as np
logger = logging.getLogger('dropsignal')

CHANNEL_NAMES = {"ID-Peak.txt": "YFP", "ID-Intensity.txt": "YFPSum", "ID-Time.txt": "Time", "Time.txt":"Time", "Timestamp.txt":"Time",
}

def detect_files(path):
    """Detect the times_series files

    Returns: a pandas dataframe with run, times and droplet counts and a dict with the different files"""

    count_file = os.path.join(path, "Count.txt")
    if not os.path.exists(count_file):
        count_file = os.path.join(path, "DropCount.txt")
    if os.path.exists(count_file):
        records = pandas.read_csv(count_file, sep="\t", header=None, error_bad_lines=False)
        records.columns = ["run", "time", "count", "direction"]
        records.time = [datetime.datetime.strptime(x, "%Y-%m-%d %H:%M") for x in records.time]
    else:
        raise ValueError("Count file {} absent".format(count_file))

    files = []
    for pth in glob.glob(os.path.join(path, "*.txt")):
        if os.path.basename(pth) not in frozenset(['Count.txt', 'DropCount.txt', 'MessageLog.txt', 'MsgLog.txt']):
            with open(pth, 'r') as file:
                length = [len(x.split("\t")) for x in file]
            fi = {"row": len(length), "path":pth, "max_col": max(length),
                  "min_col": min(length), "name":os.path.basename(pth), "use":False}
            if fi["row"]  != records.shape[0]:
                fi["status"] = "Bad line count: expected {} found {}".format(records.shape[0], fi["row"])
            elif fi["max_col"] != fi["min_col"]:
                fi["status"] = "Inconsistent number of columns"
                fi["use"] = True
            elif fi["max_col"] == fi["min_col"] == 1:
                fi["status"] = "Only one column"
            elif fi["max_col"]-1 != records["run"].nunique:
                fi["status"] = "Bad column count"
            else:
                fi["status"] = "Good"
                fi["use"] = True
            try:
                fi["channel"] = CHANNEL_NAMES[fi["name"]]
            except KeyError:
                fi["channel"] = fi["name"].split(".")[0]
            files.append(fi)
            logger.info("Found {} (Channel: {})\tShape=[{}, {}/{}]: {}{}".format(fi["name"], fi["channel"], fi["row"],
                                                                                  fi["min_col"], fi["max_col"],
                                                                                  fi["status"],
                                                                                  {True:".", False:", Dropped."}[fi["use"]]))
    return records, files


def process(files, records, output_path=".", plots=False):
    """"""
    runs = records.copy()
    runs.rename(columns={"count":"peaks", "run":"id"}, inplace=True)
    runs["time_start"] = runs.time - runs.time.min()
    runs["duration"] = datetime.timedelta(0)
    runs["duration"].values[:-1] = runs.time.values[1:] - runs.time.values[:-1]
    runs["tags"] = runs.time.apply(lambda x: [])
    runs.direction = runs.direction.apply(lambda x: {"F":1,"B":-1}[x])

    peaks = {}
    for run in runs.id:
        peaks[run] = {}

    for fi in files:
        if fi["use"]:
            with open(fi["path"], 'r') as file:
                for line in file:
                    data = [float(x) for x in line.strip().split("\t")]
                    run = int(data[0])
                    peaks[run][fi["channel"]] = pandas.DataFrame({'value':data[1:], 'id_run':range(len(data)-1)})

    # The duration of the last run is missing
    runs["duration"].values[-1] = datetime.timedelta(minutes=peaks[runs.id.values[-1]]['Time'].value.values.max() - peaks[runs.id.values[-1]]['Time'].value.values.min())

    return runs, peaks
