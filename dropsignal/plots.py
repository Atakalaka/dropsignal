"""Plots.py -- Plotting function """
import os
import logging
import io
from collections import defaultdict
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

from PIL import Image
import numpy as np

logging.getLogger('PIL').addHandler(logging.NullHandler())
logging.getLogger('PIL').setLevel(logging.CRITICAL)
logging.getLogger('PIL.PngImagePlugin').addHandler(logging.NullHandler())
logging.getLogger('PIL.PngImagePlugin').setLevel(logging.CRITICAL)
logger = logging.getLogger('dropsignal')

logger.info("Using matplotlib {} backend {}".format(matplotlib.__version__, plt.get_backend()))

COLOR = defaultdict(lambda: 'blue', YFP="orange", RED='red', JudgePoint='black', Channel1='green', Channel2='red')

def peak_detection(sig, center=None, inter=None, number=None, outdir='.', length=400, direction=1, optimize=True):
    """Plots peaks"""

    # Change the chunksize (avoid overflow error for large datasets.)
    previous_chunksize = plt.rcParams['agg.path.chunksize']
    plt.rcParams['agg.path.chunksize'] = 20000

    pk_nb = '{} (pipeline, red dots)'.format(len(center))
    if 'JudgePoint' in sig:
        center_judge = np.arange(len(sig['JudgePoint'].value))[sig['JudgePoint'].value.values > 0]
        pk_nb += '\n {} (judgePoint, black dots)'.format(len(center_judge))
        del sig['JudgePoint']
    else:
        center_judge = []
    direction_txt = '---Time--->' if direction == 1 else "<---Time---"


    nb_channels = len(sig)
    fig = plt.figure(figsize=(length, 2*nb_channels))
    for i, (channel, signal) in enumerate(sig.items(), 1):
        plt.subplot(nb_channels, 1, i)
        signal = signal.values[:, 0]
        plt.yticks([0, .5, 1], ['{:2.3}'.format(x) for x in [signal.min(),
                                                             (signal.min()+signal.max())/2,
                                                             signal.max()]])
        signal = (signal-signal.min())/(signal.max()-signal.min())
        plt.plot(signal, label=channel, color=COLOR[channel])
        plt.xlim(0, len(signal))
        plt.ylim(0, 1.1)
        plt.text(1, .85,
                 " Run {} ({})\n Peaks: {}\n      {}".format(number, channel, pk_nb, direction_txt),
                 verticalalignment='top')

        if center is not None:
            plt.scatter(center, signal[center], color='red')
            plt.xticks([])
            for i,c in enumerate(center[::2],1):
                plt.text(c,0, s=i*2, horizontalalignment='center', verticalalignment="top",fontsize=13)
            #plt.xticks(center, range(1, len(center)+1), rotation=45) # Xticks is suuuuper slow.
        if len(center_judge):
            plt.scatter(center_judge, signal[center_judge], color='k')
        if inter is not None:
            plt.vlines(inter, 0, 1, color='grey', linestyle="--")
            noise_start = signal[:inter[0]]
            noise_end = signal[inter[-1]:]
            plt.hlines(np.mean(noise_start), 0, len(signal), color='grey')
            plt.hlines(np.mean(noise_end), 0, len(signal), color='grey')
        i += 1

    plt.tight_layout()
    if outdir != None:
        filename = os.path.join(outdir, 'peakdetect_{:03}.png'.format(number))
        if optimize:
            with io.BytesIO() as ram:
                plt.savefig(ram, format='png', dpi=45, bbox_to_inches='tight')
                plt.close(fig)
                ram.seek(0)
                im = Image.open(ram)
                im2 = im.convert('RGB').convert('P', palette=Image.ADAPTIVE)
                im2.save(filename,
                         format='PNG', optimize=True, quality=75)
                del im
                del im2
        else:
            plt.savefig(filename, format='png', dpi=45)
            plt.close(fig)
    plt.clf()
    try:
        plt.close('all')
    except Exception:
        pass
    plt.rcParams['agg.path.chunksize'] = previous_chunksize  # reset chunksize.

def individual_peaks(signal, peaks, channels,  path, step_size = 1):
    '''Save a small plot per drop'''
    for n, row in peaks[list(peaks.keys())[0]].iterrows():
        try:
            fig = plt.figure(figsize=(3,3))
            ax = plt.gca()
            for _, chan in enumerate(channels):
                ax.plot(signal['time'][int(row['region_start']):int(row['region_end']):step_size],
                        signal[chan][int(row['region_start']):int(row['region_end']):step_size],
                        label='chan',
                        lw=.5)
            for i,chan in enumerate(peaks.keys()):
                ax.scatter(peaks['__measure__'].ix[n,'time'], peaks[chan].ix[n,'max'], color='k')
#                ax.vlines(signal['time'][int(row['integration_start'])],0,5,color='k')
#                ax.vlines(signal['time'][int(row['integration_end'])],0,5,color='k')
            plt.tight_layout()
            #            plt.legend()
            plt.savefig("{}_{:03}.png".format(path,int(row['id_run'])), format='png', dpi=60)
        except Exception as ex:
            logger.exception(ex)
        finally:
            plt.close(fig)
            plt.clf()


def peak_detection_binraw(signal, peaks, channels, cnames, threshold=None, direction=None):
    fig = plt.figure(figsize=(500, 2*len(channels)))
    xlim = (signal['time'].min(), signal['time'].max())
    direction_txt = {1:'---Time--->', -1:"<---Time---", None:'undirected'}[direction]
    direction = 1 if direction not in [1, -1] else direction
    two_oblocks = False

    if 'ptd1' in channels and 'ptd2' in channels:
        shift_ptd2 = peaks['__measure__']['time_ptd2'][0] - peaks['__measure__']['time_ptd1'][0]
        two_oblocks = True
        if np.isnan(shift_ptd2):
            shift_ptd2 = 0.0

    for i, chan in enumerate(channels):
        # Setup ax
        ax = plt.subplot2grid((len(channels), 1), (i, 0))
        shift_txt = ''
        ax.set_xlim(xlim)
        if two_oblocks:
            if chan in ('pmt1','pmt2','ptd1'):
                times = 'time_ptd1'
            else:
                times = 'time_ptd2'
                shift_txt = 'Shifted {:5.5}s'.format(shift_ptd2)
                ax.set_xlim(xlim[0]+shift_ptd2,xlim[1]+shift_ptd2)
        else:
            times = 'time'
        ax.set_xlim(ax.get_xlim()[::direction])
        pos_text = ax.get_xlim()[0]
        ax.set_xticks([])
        ax.spines['top'].set_visible(False)
        ax.spines['bottom'].set_visible(False)

        # Plot signal
        ax.plot(signal['time'], signal[chan], color='k', lw=.5)

        # Add title
        height_txt = 0.9*(signal[chan].max()-signal[chan].min())+signal[chan].min()
        plt.text(pos_text, height_txt,
                 " {} \n {}\n {}".format(cnames[chan], direction_txt, shift_txt),
                 verticalalignment='top')

        # Add detection threshold on the ptd channels.
        if threshold is not None and chan[1]=='t':
            height = threshold*(signal[chan].max()-signal[chan].min())+signal[chan].min()
            ax.hlines(height, pos_text, ax.get_xlim()[1], color='r')
            ax.text(pos_text, height,
                    "Detection: {}\n {} peaks".format(threshold,
                                                      peaks['__measure__'][times].count()),
                    color='r', verticalalignment='top')

        # Add droplet numbers.
        for _, row in peaks['__measure__'].iterrows():
            if not np.isnan(row[times]):
                ax.text(row[times], 0,
                        s=int(row['id_run']),
                        horizontalalignment='center',
                        verticalalignment="top", fontsize=10)

        # try:
        #     if cnames[chan] in peaks:
        #         if len(times) != len(peaks[cnames[chan]]['max'].values):
        #             raise ValueError('Incoherent number of peaks between measure and {}'.format(cnames[chan]))
        #         ax.scatter(times, peaks[cnames[chan]]['max'], color='red')
        #     else:
        #         try:
        #             pos_peak = peaks[cnames[channels[i+1]]]['center']
        #             ax.scatter(signal['time'][pos_peak], signal[chan][pos_peak], color='red')
        #         except Exception as ex:
        #             print(list(peaks.keys()))
        #             print(cnames[channels[i+1]])
        #             print(peaks[cnames[channels[i+1]]].head())
        #             print('__measure__')
        #             print(peaks['__measure__'].head())
        #         for i,row in peaks[cnames[channels[i+1]]][::2].iterrows():
        #             ax.text(times[i], 0, s=int(row['id_run']), horizontalalignment='center', verticalalignment="top",fontsize=13)
        # except Exception as ex:
        #     logger.exception(ex)
        #     pass
    plt.tight_layout()
    return fig
