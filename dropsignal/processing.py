"""Processing.py --
Signal processing.
"""
import logging

import pandas as pd
import numpy as np
import scipy.signal
import scipy.optimize

from typing import Iterable

logger = logging.getLogger('dropsignal')

def detect_peaks(sig: Iterable, window: str, window_size: int, threshold: float):
    """Return peak positions.

    Peak detection is done by a simple thresholding on a smoothed then
    normalised signal.

    Args:
        sig: light deflection signal.
        window: name of the smoothing window (cf. scipy.signal.get_window).
        window_size: size of the smoothing window. No smoothing if 0.
        threshold: min value on the the smoothed then normalised (between 0 and 1) signal
                   to consider that we are within a peak.

    Returns:
        peaks_centers: position of the center of the peaks.
        peaks_inter: position of the limits of the peaks regions.
        detection: 1 if within a peak.
        starts,ends: position of the beginning and ends of peaks.
    """
    if len(sig) < 3:
        logger.error('Signal array is too short: {}'.format(sig.shape))
        return [], [], sig, [], []

    # Signal smoothing.
    if window_size:
        win = scipy.signal.get_window(window, window_size)
        sig = scipy.signal.convolve(sig, win, mode='same') / sum(win)

    # Peak detection on the smoothed signal.
    detection = np.array(sig-sig.min() > threshold*(sig.max()-sig.min()), dtype=int)

    if detection.sum() < 1 or detection.sum() == len(detection):
        logger.error("No peak detected, dropping the run.")
        return [], [], sig, [], []

    # Get the position at which the detection mask change value.
    # Eg. 000111000 -> start = [3,] & end = [6,]
    idx = np.arange(1, len(detection))
    starts = idx[detection[1:] == detection[:-1]+1]
    ends = idx[detection[1:] == detection[:-1]-1]

    if not len(starts) or not len(ends):
        logger.error("No peak detected, Dropping the run.")
        return [], [], sig, [], []
    if len(starts) != len(ends):
        logger.error("Bad Number of peaks limits, Dropping the run.")
        return [], [], sig, [], []
    if starts[0] > ends[0]:
        logger.warning("The signal starts in a peak: Dropping first endpeak.")
        ends = ends[1:]
    if starts[-1] > ends[-1]:
        logger.warning("The signal ends in a peak: Dropping last startpeak.")
        starts = starts[:-1]

    # The peak center is in the middle of two consecutive peaks limits.
    peak_centers = (starts + ends)//2
    peak_inter = (starts[1:] + ends[:-1])//2

    # Adding the first and last peak intervals.
    # first: start[0] - (end[0] - start[0])
    # last: end[-1] + (end[-1] - start[-1])
    peak_inter = np.hstack((
        max(int(2*starts[0]-ends[0]), 0),
        peak_inter,
        min(len(detection), int(2*ends[-1]-starts[-1]))
    ))
    return peak_centers, peak_inter, detection, starts, ends


def measure_peaks(sig: Iterable, detection: Iterable, peak_inter: Iterable, alignment_correct):
    """Measure the peaks

    The `signal` is divided into regions harboring one peak by the
    `peak_inter` values. For each of those region, the maximum signal
    value is computed.

    The mean value is computed as the maximum of the mean of the
    signal over a sliding window which has the size of the droplet
    defined by light deflection (detection). This is a way to correct
    the fact that the measure point of the detection channel and
    fluorescence channels are never perfectly aligned (for hardware
    reasons).

    Args:
        sig (np.array): signal
        detection (np.array): true if the corresponding `signal` entry is in a peak.
        peak_inter (np.array): limits of the peak regions

    Returns a dataframe with a line per peak:
       id_run: droplet index
       max: max signal within each region
       mean: max of mean over the sliding window
       shift: offset between the detection and the signal
    """
    if len(sig.shape) > 1:
        sig = sig.values[:, 0]

    # Measure the max on the peak-regions
    peak_max = np.array([np.max(sig[x:y]) for x, y in zip(peak_inter[:-1], peak_inter[1:])])

    # Measure the mean as the max value of the mean over the sliding
    # window defined by detection.  This is done by taking the max of
    # the convolution of the signal with the door function in
    # detection (divided by the number of points to get a mean).
    #
    # We do this to to correct the physical offset between laser
    # detection and fluorescence measure due to (unavoidable) hardware
    # imperfections.

    #measured_points = [np.sum(detection[x:y]) for x, y in zip(peak_inter[:-1], peak_inter[1:])]
    peak_convolve = [scipy.signal.fftconvolve(sig[x:y], detection[x:y][::-1])
                     for x, y in zip(peak_inter[:-1], peak_inter[1:])]
    #peak_mean = [np.max(x)/mp for x, mp in zip(peak_convolve, measured_points)]
    # Get the magnitude of the shift induced by the local alignment of
    # detection and fluorescence.
    region_size = [abs(x-y) for x, y in zip(peak_inter[:-1], peak_inter[1:])]
    
    if alignment_correct:
        peak_shift = [np.argmax(x)-L+1 for x, L in zip(peak_convolve, region_size)]
    else:
        peak_shift = np.zeros_like(region_size,dtype=int)
    # Measure the standard deviation on the detection region.
    def get_mask(rsize, peak_shift, detection):
        mask = np.zeros(rsize)
        if peak_shift > 0:
            mask[abs(peak_shift):] = detection[:-abs(peak_shift)]
        elif peak_shift < 0:
            mask[:-abs(peak_shift)] = detection[abs(peak_shift):]
        else:
            return detection > 0
        return mask > 0
    peak_mask = [get_mask(rsize, shift, detection[x:y])
                 for rsize, shift, x, y
                 in zip(region_size, peak_shift, peak_inter[:-1], peak_inter[1:])]
    peak_std = [np.std(sig[x:y][mask])
                for mask, x, y in
                zip(peak_mask, peak_inter[:-1], peak_inter[1:])]
    peak_mean = [np.mean(sig[x:y][mask])
                for mask, x, y in
                zip(peak_mask, peak_inter[:-1], peak_inter[1:])]
    peak_area = [np.sum(sig[x:y][mask])
                for mask, x, y in
                zip(peak_mask, peak_inter[:-1], peak_inter[1:])]
    peak_median = [np.median(sig[x:y][mask])
                for mask, x, y in
                zip(peak_mask, peak_inter[:-1], peak_inter[1:])]

    out = pd.DataFrame({
        'id_run':np.arange(len(peak_max)),
        'max':peak_max,
        'mean':peak_mean,
        'area':peak_area,
        'shift':peak_shift,
        'std':peak_std,
        'median':peak_median,
    })
    out['cov'] =  out['std']/out['mean']
    out.reset_index(inplace=True, drop=True)
    return out

def measure_droplet(start, ends, times):
    """
    Measure the number of points and time of each peak within each region.
    """
    out = pd.DataFrame({
        'id_run': np.arange(len(start)),
        'measure_points': ends-start,
        'start_time': times[start].values,
        'end_time': times[ends].values,
        'start_peak':start
    })
    out['time'] = (out['start_time'] + out['end_time'])/2
    out.reset_index(inplace=True, drop=True)
    return out

def reconciliate_ptd(measures, distance=36, sampling_period=8, names=['ptd1','ptd2'], reference='ptd1'):
    """Merge the data from the two optical blocs, measure droplet speed
    and size.

    The distance between the two lasers is 36 mm +- 2mm.
    The sampling speed is 8ms per data point.
    """
    ref = measures[reference].shape[0]
    to_del = [] 
    for k,v in measures.items():
        if v.shape[0] != ref :
            logger.error('Inconsistant number of droplets for photodiode {} ({} while it should be {} like in reference photodiode {})'.format(k,v.shape[0],ref,reference))
            to_del.append(k)
        
    # number_drops = frozenset([i.shape[0] for i in measures])
    # if len(number_drops) > 1:
    #     logger.error('Inconsistant number of droplets between photodiodes: {}. Dropping the second photodiode!'.format(number_drops))
    #     merged = pd.merge(measures[0], measures[1], how='left', left_on='id_run', right_on='id_run', suffixes=['_ptd1','_ptd2'])
    #     measures[0]['time_ptd1'] = merged['time_ptd1']
    #     measures[0]['time_ptd2'] = merged['time_ptd2']
    #     measures[0]['start_time_ptd1'] = merged['start_time_ptd1']
    #     measures[0]['start_time_ptd2'] = merged['start_time_ptd2']
    #     measures[0]['end_time_ptd1'] = merged['end_time_ptd1']
    #     measures[0]['end_time_ptd2'] = merged['end_time_ptd2']
    #     return measures[0]

    keep_mean = ['time', 'measure_points']
    keep_all = ['time', 'start_time', 'end_time']

    # Keep mean between ptd1 and 2
    # and measure speed and size
    if 'ptd1' in measures and 'ptd2' in measures and measures['ptd1'].shape[0]==measures['ptd2'].shape[0]:
        grouped = pd.concat([measures['ptd1'],measures['ptd2']]).groupby('id_run')
        means = grouped.mean()
        out = means.loc[:, keep_mean]

        # measure diff between ptd1 and 2
        ranges = grouped.max() - grouped.min()

        # Speed = distance / time
        out['speed'] = distance/(ranges['start_peak']*sampling_period)

        # distance = speed * time
        out['size'] = out.speed * (out.measure_points*sampling_period)
    else:
        logger.error('Impossible to compute speed and size')
        out = measures[reference]
        
    # Keep all measures
    for name, df in measures.items():
        for col in keep_all:
            out['{}_{}'.format(col,name)] = df[col]
    return out


def concat_peaks(all_peaks):
    """Concatenate all the peaks table (used to create the signal_annotation.csv)

    all_peaks is a dict, containing an entry per run, each entry is a
    dict (one per channel + the special 'measure' channel).
    """
    data = []
    for run, peaks in all_peaks.items():
        channels = [x for x in peaks.keys() if x[0] != '_']
        data.append(peaks['__measure__'].copy())
        for chan in channels:
            data[-1] = pd.merge(data[-1],
                                peaks[chan].rename(columns=dict([(col, chan+"_"+col)
                                                                 for col in peaks[chan].columns
                                                                 if col != 'id_run'])),
                                how='left', on='id_run')
        data[-1]['run'] = run
    data = pd.concat(data).set_index(['run', 'id_run'])
    return data
