"""raw_explorer.py ~ web application serving the raw data
dropsignal - Guilhem Doulcier 2017 GPL3+

There are two objects: a cherrypy plugin that load and keep the data,
and an cherrypy exposed object that handle the requests.
"""

import os
import concurrent.futures
import logging
import json

import numpy as np
import pandas as pd
import cherrypy
from cherrypy.process import plugins

import dropsignal.processing_binraw
import dropsignal.processing
from dropsignal.processing_binraw import PMT2PTD, BINRAW_FORMAT, CHANNEL_NAMES
import dropsignal.protocol

logger = logging.getLogger('dropsignal')
logger.setLevel(logging.DEBUG)
logger.propagate = False

PMT2PTD.update({k:k for k in ('ptd1', 'ptd2', 'ptd3', 'ptd4', 'ptd5')})

class RawDataPlugin(plugins.SimplePlugin):
    """ The data is loaded from files and stored in this object,
    The request ask data to this objects via cherrypy's bus"""
    def __init__(self, bus, path, channel_prefix):
        plugins.SimplePlugin.__init__(self, bus)
        self.files = None
        self.metadata = None
        self.protocol = None
        self.path = path
        self.data = {}
        self.channel_prefix = channel_prefix
        self.executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)

    def load_file(self, run, path):
        """ Load a raw data file """
        self.data[run] = dropsignal.processing_binraw.extract(path)
        self.data[run]['time'] -=  self.protocol['T0_offset']
        self.bus.log('Loaded {} Time shifted by {}'.format(path, self.protocol['T0_offset']))

    def get_data(self, run):
        '''Load the run if needed and return the data'''
        if self.data[int(run)] is None:
            try:
                path = self.files.path[self.files.run == int(run)].values[0]
                self.load_file(int(run), path)
            except Exception as ex:
                return np.zeros(1, dtype=BINRAW_FORMAT)
        return self.data[int(run)]
    def get_data_keys(self):
        return sorted([int(x) for x in self.data])
    def get_protocol(self):
        return self.protocol
    def get_metadata(self,run=None):
        if run is not None:
            return self.metadata[self.metadata.run==int(run)]
        return self.metadata


    def start(self):
        """Called by cherry py when the plugin is started.
        Load all the data to RAM and register the data getter.
        """
        # Register the data getter to the bus.
        self.bus.subscribe(self.channel_prefix+"get_data", self.get_data)
        self.bus.subscribe(self.channel_prefix+"get_data_keys", self.get_data_keys)
        self.bus.subscribe(self.channel_prefix+"get_protocol", self.get_protocol)
        self.bus.subscribe(self.channel_prefix+"get_metadata", self.get_metadata)

        # Load protocol
        try:
            with open(os.path.join(self.path, 'analysis', 'metadata.json'), 'r') as file:
                self.protocol = json.load(file)
        except Exception as ex:
            logger.warning('Failed to find metadata.json. Loading example protocol.')
            self.protocol = dropsignal.protocol.example_protocol()
        # Load csv data.

        # droplet_dynamics contains the annotated droplets whereas
        # signal_annotation contains info about all the peaks, even
        # the ones that were discarded by quality control.
        try:
            droplet_dynamics = pd.read_csv(os.path.join(self.path, 'analysis', 'droplet_dynamics.csv')).loc[:, ['id_run', 'run', 'id_exp', 'offset']]
        except FileNotFoundError:
            logger.error('Droplet dynamics not found (droplet_dynamics.csv)')
            droplet_dynamics = None
        try:
            signal_annotation = pd.read_csv(os.path.join(self.path, 'analysis', 'signal_annotation.csv'))
        except FileNotFoundError:
            logger.error('Signal annotation not found (signal_annotation.csv)')
            signal_annotation = None
        if droplet_dynamics is not None and signal_annotation is not None:
            self.metadata = pd.merge(droplet_dynamics, signal_annotation, on=['run','id_run'], how='right')
            self.metadata.id_exp.fillna('-1', inplace=True)
        elif droplet_dynamics is None and signal_annotation is not None:
            self.metadata = signal_annotation
            self.metadata['id_exp'] = -1
        elif droplet_dynamics is not None and signal_annotation is None:
            self.metadata = droplet_dynamics
        else:
            self.metadata = pd.DataFrame([])

        # Some peaks are not associated to a droplet
        try:
            self.protocol['droplet_to_well']['-1'] = 'Unknown'
            self.protocol['well_to_group']['Unknown'] = 'Unknown'
            for well in self.protocol['droplet_to_well'].values():
                if well not in self.protocol['well_to_group']:
                    self.protocol['well_to_group'][well] = 'Unknown'

        except (KeyError, TypeError) as ex:
            logger.warning('Old version of protocol detected')

        # Discover the file
        self.files = dropsignal.processing_binraw.discover(self.path)
        for _, row in self.files.iterrows():
            self.data[row.run] = None
            # Asynchronously load some raw data file in RAM.
            # self.executor.submit(self.load_file, row.run, row.path)

    def stop(self):
        self.executor.shutdown()
        self.bus.unsubscribe(self.channel_prefix+"get_data", self.get_data)
        self.bus.unsubscribe(self.channel_prefix+"get_data_keys", self.get_data_keys)
        self.bus.unsubscribe(self.channel_prefix+"get_protocol", self.get_protocol)
        self.bus.unsubscribe(self.channel_prefix+"get_metadata", self.get_metadata)
        self.unsubscribe()
        self.bus.log('Stopping down RAW DATA PLUGIN')


@cherrypy.expose
class MetadataGetter(object):
    def __init__(self,channel_prefix):
        self.channel_prefix = channel_prefix

    @cherrypy.tools.accept(media='application/json')
    @cherrypy.tools.json_out()
    def GET(self, run='list'):
        """ Get information about a run (or all the runs if run is 'list')"""
        protocol = cherrypy.engine.publish(self.channel_prefix+'get_protocol')[0]
        if run == 'list':
            channels = [x[1] for x in protocol['channels_names']
                        if (x[0] in protocol['active_channels'])]
            channels += list(frozenset([CHANNEL_NAMES[PMT2PTD[x]] for x in protocol['active_channels']]+['detection_head']))
            return {
                'channels': channels,
                'runs': cherrypy.engine.publish(self.channel_prefix+'get_data_keys')[0],
                'coalescence_events': protocol['coalescence_event_list'],
                'color_group':protocol['color_group']}

        data = cherrypy.engine.publish(self.channel_prefix+'get_data', run)[0]
        metadata = cherrypy.engine.publish(self.channel_prefix+'get_metadata', run)[0]

        try:
            peaks = [{'group':
                      protocol['well_to_group'][protocol['droplet_to_well'][str(int(row.id_exp))]],
                      'time':row.time}
                     for _, row in metadata.iterrows()]
        except Exception as ex:
            logger.exception(ex)
            peaks = []
        return {
            'size':data.shape[0],
            't0':data.time.min(),
            'tf':data.time.max(),
            'peaks': peaks
        }

@cherrypy.expose
class SignalGetter(object):
    def __init__(self,channel_prefix):
        self.channel_prefix = channel_prefix

    @cherrypy.tools.accept(media='application/json')
    @cherrypy.tools.json_out()
    def GET(self, t0=None, tf=None, run=None, channel=None, skip=1, drop=None):
        """Get the right chunck of signal.

        Args:
        - t0,tf (float): Initial and final times
        - run (int): Run number
        - channel (str): Channel name
        - skip (int): Iteration step for the signal (allow to load less data when >1)
        - drop (int): identification of the drop (id_exp) we want to be in the center.

        If t0 and tf are specified, get a time window of the signal
        If drop is specified, get a window around the given drop
        """

        print('Signal request: t0={}, tf={}, run={}, channel={}, skip={}, drop={}'.format(t0,tf,run,channel,skip,drop))

        run = int(run)

        protocol = cherrypy.engine.publish(self.channel_prefix+'get_protocol')[0]
        data = cherrypy.engine.publish(self.channel_prefix+'get_data', run)[0]
        metadata = cherrypy.engine.publish(self.channel_prefix+'get_metadata', run)[0]


        # Get the channel name
        # Optionally remove the suffix '_max,_mean'... so links from the scatter works.
        cname_to_pmtptd = {v:k for k,v in protocol['channels_names']}
        cname_to_pmtptd['detection_1']= 'ptd1'
        cname_to_pmtptd['detection_2']= 'ptd2'
        cname_to_pmtptd['detection_scatter']= 'ptd5'
        cname_to_pmtptd['detection_scattering']= 'ptd5'
        cname_to_pmtptd['detection_head']= 'ptd4'
        pmtptd_to_cname = {v:k for k,v in cname_to_pmtptd.items()}
        if channel in PMT2PTD:
            ptd = PMT2PTD[channel]
        else:
            if channel in cname_to_pmtptd:
                ptd = PMT2PTD[cname_to_pmtptd[channel]]
            else:
                channel = '_'.join(channel.split('_')[:-1])
                if channel in cname_to_pmtptd:
                    ptd = PMT2PTD[cname_to_pmtptd[channel]]
                else:
                    ptd = 'ptd1'
                    channel = pmtptd_to_cname['ptd1']

        time_col = 'time_'+ptd
        if time_col not in metadata.columns:
            logger.debug('time_col '+time_col+' not found using time instead !')
            time_col = 'time'
        start_col = 'start_time_'+ptd
        if start_col not in metadata.columns:
            logger.debug('time_col '+start_col+' not found using start_time instead !')
            start_col = 'start_time'
        end_col = 'end_time_'+ptd
        if end_col not in metadata.columns:
            logger.debug('time_col '+end_col+' not found using end_time instead !')
            end_col = 'end_time'

        # Parse t0 and tf.
        try:
            t0 = float(t0)
        except (ValueError,TypeError):
            t0 = data.time.min()
        if t0 <= 0:
            t0 = data.time.min()
        if tf=='end':
            tf = data.time.max()
        else:
            try:
                tf = float(tf)
            except (ValueError,TypeError):
                tf = data.time.min() + (data.time.max()-data.time.min())/100
        if tf<t0:
            tf,t0 = t0,tf
        dt = data['time'].values[1] - data['time'].values[0]

        # Select an unique droplet
        if drop is not None:
            drop = int(drop)
            droplet =  metadata[metadata['id_exp'] == drop]
            if droplet.shape[0] >= 1:
                element = droplet.iloc[0,:].to_dict()
                t0 = element[time_col] - 30*element['measure_points']*dt
                tf = element[time_col] + 30*element['measure_points']*dt
            else:
                dist = np.abs(metadata.id_exp-drop)

                # If there is no id_exp (non annotated run) use id_run.
                if np.unique(metadata.id_exp)[0] == -1:
                    dist = np.abs(metadata.id_run-drop)

                dist.sort_values(inplace=True)
                droplet =  metadata.loc[dist.index[:1], ['id_exp', time_col, 'measure_points']]
                if droplet.shape[0] >= 1:
                    element = droplet.iloc[0, :].to_dict()
                    t0 = element[time_col] - 30*element['measure_points']*dt
                    tf = element[time_col] + 30*element['measure_points']*dt
                else:
                    t0 = data.time.min()
                    tf = data.time.min() + (data.time.max()-data.time.min())/100

        peaks_in_frame = metadata[(metadata[time_col] > t0) & (metadata[time_col] < tf)]
        print('Asking for run {} frame {} to {} ({}/{} peaks)'.format(run,t0,tf,peaks_in_frame.shape[0], metadata.shape[0]))
        peaks_in_frame = peaks_in_frame.fillna(0)

        # Prepare signal response
        if skip != 'all':
            skip = int(skip)
            signal = [(float(x), float(y)) for x, y
                      in data[data.time.between(t0, tf)].loc[::skip, ['time', cname_to_pmtptd[channel]]].values]
        else:
            signal = []
        # Prepare peaks response
        peaks = []
        for _, row in peaks_in_frame.iterrows():
            peaks.append({
                'time':float(row[time_col]),
                'start':float(row[start_col]),
                'end':float(row[end_col]),
                'id_run':int(row.id_run),
                'id_exp':int(row.id_exp),
                'well': protocol['droplet_to_well'][str(int(row.id_exp))],
                'group': protocol['well_to_group'][protocol['droplet_to_well'][str(int(row.id_exp))]],
            })
            if channel.split('_')[0] != 'detection':
                peaks[-1].update({
                    'value':float(row['{}_mean'.format(channel)]),
                    'shift':float(row['{}_shift'.format(channel)]*float(dt)),
                    'max':float(row['{}_max'.format(channel)]),
                    'median':float(row['{}_median'.format(channel)]),
                    'std':float(row['{}_std'.format(channel)]),
                    'cov':float(row['{}_cov'.format(channel)]),
                    'area':float(row['{}_area'.format(channel)]),
                })
        return {'data':signal,
                'peaks':peaks}
