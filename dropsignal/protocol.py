""" protocol.py - Protocol metadata manipulation
This file is part of the dropsignal package.
Copyright 2016 Guilhem Doulcier, Licence GNU AGPL3+
"""
import os
import logging
import json
import platform
import time
from collections import defaultdict

import pandas as pd
import numpy as np

from dropsignal import __version__, APP_PATH
logger = logging.getLogger('dropsignal.protocol')
LOCAL_DEFAULT_PATH = os.path.join(os.path.join(APP_PATH, 'local_protocol.json'))
WEB_OPTIONS = {
    "web_initial_port":8080,
    "web_plugin_list":['train_heatmap', 'poisson'],
    "web_browser":False,
    "web_firefox_path":"C:\\Program F~\\Mozilla Firefox\\firefox.exe",
    "web_default_path":".",
    "web_public":False,
}

def protocol_read(protocol_path):
    protocol = {}
    # Load the protocol.json
    try:
        # Remove empty lines and comments.
        text = [x.strip()
                for x in open(protocol_path).readlines()
                if not x.startswith("#")
                and len(x.strip())]
        try:
            protocol.update(json.loads('{\n'+',\n'.join(text)+'\n}'))
        except ValueError as ex:
            protocol.update(json.loads('\n'.join(text)))
    except Exception as ex:
        logger.warning(('Failed to load experimental protocol: '
                        '{}, {}').format(protocol_path, ex))
    else:
        logger.info("Reading protocol in {}".format(protocol_path))

    # check input correctness
    if 'manual_coalescence_events' in protocol and protocol['manual_coalescence_events']:
        try:
            for event in protocol['manual_coalescence_events']:
                assert event[0] == int(event[0])
                try:
                    assert event[1] == int(event[1])
                except TypeError:
                    for i in event[1]:
                        assert i == int(i)
        except (IndexError, TypeError, AssertionError):
            logger.error('Option manual_coalescence_events is badly formated shoud be a list of `event` [event, event...] with the format event=[run,drop] or event=[run, [drop,drop...]] (`run` and `drop` are integers)')
            protocol['manual_coalescence_events'] = []
            logger.warning('manual_coalescence_events ignored')
    return protocol

def template_reader(template_path: str):
    if template_path is not None and os.path.exists(template_path):
        try:
            logger.info("Reading template in {}".format(template_path))
            template = pd.read_csv(template_path, comment='#').fillna(1)
            template.columns = [str.lower(str(x)) for x in template.columns]
            required_columns = ["description", 'well']
            if any([x not in template.columns for x in required_columns]):
                raise ValueError("Missing one of the required column: {}".format(required_columns))
        except Exception as ex:
            logger.warning("Failed to load the template: {}".format(ex))
            template = pd.DataFrame()
    else:
        template = pd.DataFrame()
    return template

def loader(protocol_path: str, template_path: str = None):
    """Build the protocol dict by loading data fron the protocol.json and
    template.csv files. See the documentation for the syntax of those
    files.
    """
    # Load the dropsignal default.
    protocol = {k:v['default'] for k, v in PROTOCOL_DOC.items() if 'default' in v}

    # Read the file
    protocol.update(protocol_read(protocol_path))

    # Add pipeline info
    protocol["pipeline_infos"] = {'host': "|".join(platform.uname()),
                                  "timestart": time.asctime(),
                                  "version": __version__}
    protocol["status"] = "Running"

    # Read the template
    template = template_reader(template_path)

    # Get the order in which the wells are visited
    if 'order' in template.columns:
        order = template.sort_values('order').well
        if 'well_order' in protocol:
            logger.debug("`well_order` found in template, ignoring `well_order` value in protocol")
            del protocol['well_order']
    else:
        if protocol['well_order'] not in WELL_ORDER:
            logger.error("Unknown well order: {}, using {}.".format(protocol['order'],
                                                                    PROTOCOL_DOC['well_order']['default']))
            protocol['well_order'] = PROTOCOL_DOC['well_order']['default']
        order = WELL_ORDER[protocol['well_order']]


    # Get the number of droplets in each well
    if 'droplet_number' in template.columns:
        droplets_per_well = template.set_index('well').ix[order,'droplet_number'].values
        protocol["expected_droplets"] = template.droplet_number.sum()
        if 'droplets_per_well' in protocol:
            logger.debug("`droplet_number` found in template, ignoring `droplet_per_well` value in protocol")
            del protocol['droplets_per_well']
    else:
        droplets_per_well = protocol['droplets_per_well']

    # Build the droplet<->well mappings
    protocol["droplet_to_well"], protocol["well_to_droplets"] = droplet_mapping(droplets_per_well, order, protocol['visit_mode'])
    protocol["expected_droplets"] = len(protocol["droplet_to_well"])
    logger.info("Expecting a {} droplets train".format(protocol['expected_droplets']))

    # Add groups, tags and dillutions.
    if len(template):
        protocol["droplets"] = template_to_droplets(template, protocol['well_to_droplets'])
    if "droplets" not in protocol:
        protocol["droplets"] = []

    # Group colors (and well labels)
    protocol['well_to_group'] = {}
    color_usage = {x:0 for x in protocol['color_palette']}
    for k, v in protocol['color_group'].items():
        if v[0] == 'C':
            protocol['color_group'][k] = protocol['color_palette'][int(v[1:])]
        elif v[0] != '#':
            less_used_color = sorted(color_usage.items(), key=lambda x:(x[1],x[0]))[0][0]
            protocol['color_group'][k] = less_used_color
        color_usage[protocol['color_group'][k]] += 1
    for group in sorted(protocol['droplets'],key=lambda x:x['label']):
        for well in group['well']:
            protocol['well_to_group'][well] = group['label']
        if group['label'] not in protocol['color_group']:
            less_used_color = sorted(color_usage.items(), key=lambda x:(x[1],x[0]))[0][0]
            protocol['color_group'][group['label']] = less_used_color
            color_usage[less_used_color] += 1

    # Output path.
    if "output_path" not in protocol:
        protocol["output_path"] = os.path.join(os.path.dirname(protocol_path), 'analysis')
    # logger.debug('DROP -> WELL')
    # for k,v in sorted(protocol["droplet_to_well"].items(),key=lambda x:x[0]):
    #     logger.debug("{}: {}".format(k,v))
    # logger.debug('WELL -> Drop')
    # for k,v in sorted(protocol["well_to_droplets"].items(),key=lambda x:(x[0][0],int(x[0][1:]))):
    #     logger.debug("{}: {}".format(k,v))
    return protocol, template

def snake_order(rows, columns, back_to_init=True):
    """Return an ordering of the wells "in a snake shape" (to the end of a
    row then starting the next row from the end). Well names are the
    concatenation of a row and column name.

    Args:
        rows (iter): Rows names.
        columns (iter): Colums names.
        back_to_init (bool): If true go back to the first well at the end.
    """
    order = []
    for i, row in enumerate(rows):
        if i%2 == 0:
            direction = 1
        else:
            direction = -1
        for col in columns[::direction]:
            order.append("{}{}".format(row, col))
    if back_to_init:
        order.append(order[0])
    return tuple(order)

WELL_ORDER = {}
WELL_ORDER["snake96"] = snake_order('ABCDEFGH', range(1, 13))
WELL_ORDER["snake96_noinit"] = snake_order('ABCDEFGH', range(1, 13),back_to_init=False)
WELL_ORDER["snake90"] = snake_order('ABCDEFGH', range(1, 13),back_to_init=False)[3:-3]
WELL_ORDER["snake94"] = snake_order('ABCDEFGH', range(1, 13),back_to_init=False)[1:-1]

def droplet_mapping(droplet_per_well, order, visit_mode):
    """Return two dictionnaries that maps droplets_id->well and well->droplet_id.

    Args:
        n (int or iterable): number of droplets made by well.
        order (iterable): order in which the wells are visited.
        visit_mode (str): 'sequential' or 'hiccup'. cf visit mode in the protocol file.
    """
    droplet_to_well = defaultdict(lambda: '00')
    well_to_droplet = defaultdict(list)

    possible_visit_modes = ('sequential','hiccup')
    if visit_mode not in possible_visit_modes:
        raise ValueError('Unknown visit mode: {} (should be one of: {})'.format(visit_mode, possible_visit_modes))

    try:
        droplet_per_well[1]
    except TypeError:
        # droplet_per_well is not iterable
        for i, well in enumerate(order):
            if visit_mode=='sequential':
                drops_id =  list(range((i*droplet_per_well), (i+1)*droplet_per_well))
            elif visit_mode=='hiccup':
                drops_id = list(range((i-i%2)*droplet_per_well+i%2, (i-i%2)*droplet_per_well+i%2+2*droplet_per_well, 2))
            well_to_droplet[well] += drops_id
            for i in drops_id:
                droplet_to_well[i] = well
    else:
        # droplet_per_well is iterable
        current = 0
        if visit_mode=='sequential':
            for rank, (well, nb) in enumerate(zip(order, droplet_per_well)):
                drops_id = list(range(current, current+nb))
                current += nb
                well_to_droplet[well] += drops_id
                for i in drops_id:
                    droplet_to_well[i] = well
        elif visit_mode=='hiccup':
            ## Wells are visited by pairs.
            ## All the drops to be made in a pair
            ## are exhausted before going to the next pair.
            for (well1,ndrops1),(well2,ndrops2) in zip(zip(order[::2],droplet_per_well[::2]),
                                                       zip(order[1::2],droplet_per_well[1::2])):
                if ndrops1 != ndrops2:
                    logger.warning(('Well pair: ({}, {}) has two different number'
                                    'of drops ({},{}) while in hiccup mode.'
                                    '').format(well1,well2,ndrops1,ndrops2))
                    warning = []
                    drop0 = current
                to_add = [ndrops1, ndrops2]
                for i in range(ndrops1+ndrops2):
                    if (i%2 == 0 and to_add[0]) or (i%2 == 1 and not to_add[1]):
                        well_to_droplet[well1] += [current]
                        droplet_to_well[current] = well1
                        to_add[0] -= 1
                        if ndrops1 != ndrops2:
                            warning.append(well1)
                    elif (i%2 == 1 and to_add[1]) or (i%2 == 0 and not to_add[0]):
                        well_to_droplet[well2] += [current]
                        droplet_to_well[current] = well2
                        to_add[1] -= 1
                        if ndrops1 != ndrops2:
                            warning.append(well2)
                    current += 1
                if ndrops1 != ndrops2:
                    logger.warning('The actual order between drop {} and {} will be: {}.'.format(drop0, current,' '.join(warning)))
    return droplet_to_well, well_to_droplet

def template_to_droplets(template, rule, default=None):
    """ Get a list of droplet groups from the template

    Args:
        template (pd.Dataframe): required collums are Description, Well and Dillution,
        default (str): label of the default id.
        well_to_drop (dict): indices of the droplets when given a well.
    """
    def empty():
        """An empty droplet group"""
        return {'id':[], 'expected_growth': False, 'well': [], 'dilution': 1}

    droplets = defaultdict(empty)
    for _, df in template.reset_index().iterrows():
        if "dilution" in template.columns and template.dilution.nunique()>1:
            label = "{} [{:1.1e}]".format(df.description, df.dilution)
            droplets[label]['dilution'] = df.dilution
        else:
            label = df.description
        droplets[label]['label'] = label
        droplets[label]['well'].append(df.well)
        if "expected_growth" in template.columns:
            droplets[label]['expected_growth'] = bool(df.expected_growth)
        droplets[label]['id'].append(rule[df.well])
    if default is not None:
        droplets[default]['id'] = 'default'
    return list(droplets.values())

PROTOCOL_DOC = {
    'enabled_plugins': {'description': "List of plugins that will be run at the end of each pipeline execution",'basic':False,
                        "category":'Plugins',
                        'default':('train_heatmap',)},
    'title': {'description': "experiment title","category":'Metadata','basic':False,},
    'droplets_per_well': {'default':10,
                          'basic':False,
                          'description':'Number of droplet per well, ignored if the template file contains a `droplet_number` column',
                          "category":'Template'},
    'relative_times':{'category':'Detection', 'default':False, 'basic':True, 'description': 'if True, the column time will refer to times relative to the droplet creation, otherwise it will be the absolute time. In both cases, you will have access to absolute_time and relative_time in the text output, but the column time is the one used by the interface and plugins.'},
    'visit_mode': {'default':'sequential',
                   'basic':True,
                   'valid_values':('sequential','hiccup'),
                   'description':('Way the wells are visited. `sequential`: the wells are visited sequentially in the order supplied, making droplet_per_well droplets before moving on. `hiccup`: the wells are visited by non-overlapping pairs alterning a droplet from each one, until droplet_per_well droplets are done per well before moving on to the next pair'),
                   "category":'Template'},
    'well_order': {'default': 'snake94',
                   'description':'Order in which the wells are visited by the sipper, ignored if the template file contains an `order` column',
                   'valid_values':WELL_ORDER.keys(),
                   'basic':False,
                   'category':'Template'},
    'color_palette': {'default': ['#4e79a7',"#f28e2b","#e15759","#76b7b2","#59a14f","#edc948","#b07aa1",'#ff9da7',"#9c755f","#bab0ac"],
                      'description':'Color palette used to assign colors to groups',
                      'basic':False,
                      'category':'Template'},
    'color_group': {'default': {'empty':'C9','Empty':'C9'},
                    'description':'Use this option to fix the color associated with a group (from template). Valid entries are hexadecimal color or CX where X is the index in the color palette (starting from 0)',
                    'basic':False,
                    'category':'Template'},
    'window': {'description':'Shape of the window used to filter the droplet detection channel. See https://docs.scipy.org/doc/scipy/reference/signal.html#window-functions','basic':False, 'default':'boxcar', "category": "Detection"},
    'window_size': {'description':'Size (in measure points) of the smoothing window', 'default':10, "category": "Detection",'basic':True,},
    'detection_threshold': {'description':'Threshold for droplet detection (normalised units).','default':.1, "category": "Detection",'basic':True,},
    "active_channels" :  {'description':'Channels to extract.','default': ('pmt1','pmt2'), "category": "Detection",'basic':True,},
    "channels_names": {'description':'User defined names of channels', 'default': (('pmt1','fluo_1'),('pmt2','fluo_2'),('pmt3','fluo_3'), ('pmt4','fluo_4'),('pmt5','scattering')),'basic':False, 'category':'Detection'},
    "correct_alignment_with_detection": {'description':'Channels in which each peak must be re-aligned with the detection signal using maximum of convolution', 'basic':False, 'default':('pmt1','pmt2','pmt3','pmt4'), 'category':'Detection'},
    "reference_photodiode":{'description':'Photodiode used as reference for the number of drops. Measurments of photodiodes with a different number of drops will be dropped.','default':'ptd1','category':'Detection','basic':False,},
    "manual_coalescence_events" : {'description':'A list of Coalescence event using the format [run, id_drop]. Example: [[4,459],[5,938]] to indicate that the droplet 459 coalesced at run 4 and droplet 938 at run 5. In practice, starting from run 4 the 459th peak will be assigned to the 460th droplet (instead of 459th) and so on (You should NOT add coalescence to the first analyzed run, use the template file to manage a bad train generation). The offset is cumulative: starting at run 5 the 938th peak will be assigned to the 940th droplet. Multiple coalescence in one run can be indicated like this [run, [id_drop1,id_drop2]]. (Warning: the number in the signal plots is the uncorrected index of the peak)','default':[], "category": "Droplet coalescence",'basic':False},
    "automatic_coalescence_events" : {'description':'Use a simple heuristic to correct coalescence events','default':False, "category": "Droplet coalescence",'basic':False},
    "max_coalescence" : {'description':'Maximum number of coalesced droplets between two run under which automatic coalescence detection is performed','default':2, "category": "Droplet coalescence",'basic':False,},
    "export_xls" : {'description':'Create a consolidated results.xlsx file','default':False, "category": "Export",'basic':False,},
    "individual_csv" : {'description':'Export individual droplet dynamics in the droplet folder','default':True, "category": "Export",'basic':False},
    "advanced_detection_parameters" : {'description':'Change detection parameters for specific channels. Format: `{"CHANNEL":{"PARAMETER":VALUE, ...}, ...}`.  Example:{"ptd1":{"threshold":0.3, "window_size":20}, "ptd5":{"threshold":0.5}}. Authorised keys are: "threshold","window","window_size". This option overrule the general parameters for the given channels.','default':{}, 'category':'Detection', 'basic':False}
}

if os.path.exists(LOCAL_DEFAULT_PATH):
    for k, v in protocol_read(LOCAL_DEFAULT_PATH).items():
        if k in PROTOCOL_DOC:
            PROTOCOL_DOC[k]['default'] = v
        if k.split('_')[0] == 'web':
            WEB_OPTIONS[k] = v

def example_protocol(doc=PROTOCOL_DOC):
    """Return an example protocol.json file"""
    header = """# This is a configuration file for dropsignal {version}.
# Use it to configure the pipeline to your needs. Read the documentation for more informations.
# Entries should be valid json (http://json.org/), lines starting with "#" will be skipped.
""".format(version=__version__)
    content = ''

    for cat in sorted(frozenset([x["category"] for x in doc.values()])):
        content += "#"+"-"*15 +' ' + cat +' '+ "-"*15 +'#\n'
        for key, value in doc.items():
            if value["category"] == cat:
                content += '# {}\n'.format(value['description'])
                if 'default' in value:
                    content += '#{}\n\n'.format(json.dumps({key:value['default']})[1:-1])
                else:
                    content += '#{}\n\n'.format(json.dumps({key:''})[1:-1])
    import dropsignal.plugins
    content+=dropsignal.plugins.config()
    return header +'\n'+ content + '\n'

def example_template(order=WELL_ORDER[PROTOCOL_DOC['well_order']['default']]):
    """Return an expample template.csv file"""
    header = """# This is an example template file for dropsignal {version}.
# Required Columns are: `description` (str), `well` (str)
# Optional Columns are `order` (int), `droplet_number` (int),
# Column names are case insensitive, lines starting with "#" will be skipped.\n""".format(version=__version__)
    df = pd.DataFrame({"well":order,
                       "order":range(len(order)),
                       "droplet_number":[10]*len(order),
                       "description":["Empty"]*len(order)})
    df["well_sort"] = df.well.apply(lambda x: (x[0], int(x[1:])))
    df.sort_values('well_sort', inplace=True)
    csv = df.loc[:, ["well", "description",
                     "order", "droplet_number"]].to_csv(None, index=None)
    return header + csv
