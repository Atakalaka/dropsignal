""" A simple Plug-in system for dropsignal """
import logging
import importlib
import pkgutil
import os
import json
import pandas as pd
import dropsignal.protocol
from dropsignal import PYINSTALL

logger = logging.getLogger('dropsignal.plugins')

# list installed plugins
if PYINSTALL:
    from dropsignal.protocol import WEB_OPTIONS
    INSTALLED = WEB_OPTIONS['web_plugin_list']
else:
    INSTALLED = tuple([name for loader, name, is_pkg in pkgutil.walk_packages(__path__) if is_pkg])

def apply_plugin(name, data, protocol):
    """ Apply the main function of the plugin <name>"""
    try:
        plugin = importlib.import_module(".plugins."+name, package="dropsignal")

        # Load default parameters and override them with the values in protocol
        parameters = {k:v['default'] for k, v in plugin.PARAMETERS.items() if 'default' in v}
        parameters.update({".".join((k.split(".")[2:])): v
                           for k, v in protocol.items()
                           if k[:6] == 'plugin' and k.split(".")[1] == name})
        parameters['export_path'] = os.path.join(protocol['output_path'], 'plugins', name)
    except Exception as ex:
        logger.exception("Error in loading plugin {} : {}".format(name, ex))
    else:
        if not os.path.exists(parameters['export_path']):
            os.makedirs(parameters['export_path'])
        try:
            plugin.main(data, protocol, parameters)
        except Exception as ex:
            logger.exception("Error in plugin {} : {}".format(name, ex))

def load_data_and_apply(path, name):
    """Load the dataframes from analysis/*.csv and protocol and then apply one plugin.
       (so you do not have to start the whole pipeline again)"""
    protocol, _ = dropsignal.protocol.loader(protocol_path=os.path.join(path, 'protocol.json'))
    data = {"dynamics": pd.read_csv(os.path.join(path, 'analysis', 'droplet_dynamics.csv')),
            "droplets": pd.read_csv(os.path.join(path, 'analysis', 'droplet.csv')),
            "runs": pd.read_csv(os.path.join(path, 'analysis', 'runs.csv'))}
    logger.info("Applying plugin {}".format(name))
    apply_plugin(name, data, protocol)

def config(plugins=INSTALLED):
    content = ""
    content += "#"+"-"*15 +' ' + 'Plugins' +' '+ "-"*15 +'#\n'
    for name in plugins:
        content += "#"+"-"*3 +' ' + name +' '+ "-"*3 +'#\n'
        plugin = importlib.import_module(".plugins."+name, package="dropsignal")
        for key, value in plugin.PARAMETERS.items():
            content += '# {}\n'.format(value['description'])
            if 'default' in value:
                content += '#{}\n\n'.format(json.dumps({"plugin."+name+'.'+key:value['default']})[1:-1])
            else:
                content += '#{}\n\n'.format(json.dumps({"plugin."+name+'.'+key:''})[1:-1])
    return content

def config_dict(plugins=INSTALLED):
    conf = {}
    for name in plugins:
        plugin = importlib.import_module(".plugins."+name, package="dropsignal")
        for key, value in plugin.PARAMETERS.items():
            conf["plugin."+name+'.'+key] = {
                'description':value['description'],
                'category': "plugin."+name,
                'default': value['default'] if 'default' in value else '',
                'basic':value['basic'] if 'basic' in value else True,
            }
    return conf
