# images.py some
import glob as glob
import logging
import os
#from io import BytesIO
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

logger = logging.getLogger('dropsignal.processing')

try:
    from PIL import Image, ImageDraw
except ImportError as e:
    logger.critical("Pillow is absent, image analysis will not work")
    Image = None

def plot(img, length, start,end,thres, med,f = None):
    fig = plt.figure(figsize=(15,7))
    plt.axis('off')
    fig.subplots_adjust(bottom=0)
    fig.subplots_adjust(top=1)
    fig.subplots_adjust(right=1)
    fig.subplots_adjust(left=0)
    plt.imshow(img,cmap='bone',interpolation='none')
    plt.plot(med,color='yellow')
    plt.xlim(0,img.shape[1])
    plt.ylim(0,img.shape[0])
    plt.xticks([])
    plt.yticks([])
    plt.hlines(thres[0],0,img.shape[1],color='r')
    plt.hlines(thres[1],0,img.shape[1],color='r')
    plt.fill_betweenx(range(img.shape[0]),start,end,color='red',alpha=.33)

    #    ram = BytesIO()
    plt.savefig(f,bbox_inches='tight', pad_inches=0, )
    #    plt.savefig(ram, format='png',bbox_inches='tight', pad_inches=0)
    #    ram.seek(0)
    #    image = Image.open(ram).convert('RGB').convert('P', palette=Image.ADAPTIVE)
    #    image.save( f , format='PNG')
    plt.close()

def crop(data,thres=0.9):
    z = np.mean(data,1)
    mask = z < thres*z.max()
    return data[mask,:]

def measure(img,thres=0.5):
    x = np.median(img,0)
    M = np.median(x)
    L = np.diff(x-M>thres*np.max(x-M))
    try:
        start,end = np.arange(len(L))[L==1]
        length = end-start
    except:
        start, end, length = 0,0,0
    return length,(start,end),(M,0.5*np.max(x-M)),x

def makemosaic(files, names, start_end, outfile, title, columns=None, thumbnail=(200,100), crop=(150,120)):
    mosaic = None
    
    if columns is None:
        columns = min(50, max(len(files)//10,1))
    
    for i, (file, name, (start,end)) in enumerate(zip(files,names,start_end)):
        # Crop + thumbnail 
        im = Image.open(file)
        x,y = im.size
        draw = ImageDraw.Draw(im)
        draw.line([(start,y),(start,0)], fill=0,width=4)
        draw.line([(end,y),(end,0)], fill=0, width=4)

        im = im.crop((0,crop[0],x,y-crop[1]))
        im.thumbnail(thumbnail)

        # Create the big picture
        if  mosaic is None:
            w,h = im.size
            newsize = int(h*(np.ceil((len(files)+1)/columns))),w*columns
            logger.debug("Mosaic image: {} ({} cols, {} imgs {}x{})".format(newsize, columns, len(files),h, w))
            mosaic = np.zeros(newsize) 

        # add some text.
        draw = ImageDraw.Draw(im)
        draw.text((0,h/3), name, fill=None, font=None, anchor=None)

        # paste the picture in the mosaic
        yy = (i//columns) * h
        xx = (i%columns) * w
        try:
            mosaic[yy:yy+h,xx:xx+w] =  np.asarray(im, dtype=np.uint8)
        except Exception as ex:
            logger.error("Mosaic failed on {} in {} {} (size: {}) {}".format(name, xx, yy, im.size, ex))
            
    # Save the big picture
    im = Image.fromarray(mosaic).convert('L')
    draw = ImageDraw.Draw(im)
    W,H = im.size
    draw.text((W-w,H-h/2), title, fill=255, font=None, anchor=None)
    im.save(outfile)

def process_files(path, plot_all = False, output_path=None):
    df = []
    output_path = path if output_path is None else output_path
    files = list(glob.glob(os.path.join(path, "*.tiff")) + glob.glob(os.path.join(path, "*", "*.tiff")))
    logger.info("{} images found.".format(len(files)))
    if not len(files):
        return None

    mean_length = 0
    var_length = 0
    plot_files = []
    for n, file in enumerate(files,1):
        # Extract info from filename
        direction, id_run, run, date = os.path.basename(file).split(".")[0].split("_")
        name = os.path.basename(file)

        # Measure on image
        im = Image.open(file)
        data = np.asarray(im, dtype=np.uint8)
        cropped = crop(data)
        length, (start, end), (median, thres), med = measure(cropped)

        # Online mean var computation
        temp = mean_length
        mean_length = mean_length+(length-mean_length)/n
        var_length = ((n-1)*var_length + (length-temp)*(length-mean_length))/n

        # Quality Control
        tags = []
        plot_this = False

        if length == 0:
            logger.warning("Droplet {} run {}: length was not measured".format(id_run, run))
            tags.append("nolength")
            #plot_this = True
        if abs((length-mean_length)/np.sqrt(var_length)) > 2:
            logger.warning("Droplet {} run {}: length was unexpected".format(id_run, run))
            #plot_this = True
            tags.append("flagged_length")

        # Save data
        df.append({"path":file, "length": length,
                   "direction":direction, 'id_run': int(id_run),
                   'run': int(run), "date": pd.to_datetime(date, format='%Y%m%d-%H%M%S'),
                   "start":start, "end": end,
                   "median": median, "thres": thres, "tag":tags})

        # Plot
        if plot_this or plot_all:
            plot_file = os.path.join(output_path, name.replace(".tiff", ".jpg"))
            plot(cropped, length, start, end, (median,thres), med, plot_file)
            plot_files.append(name.replace(".tiff", ".jpg"))
            
    df = pd.DataFrame(df)
    df.sort_values(by=["id_run","run"], inplace=True)
    for run, dd in df.groupby('run'):
        dd = dd.sort_values('id_run')
        try:
            makemosaic(dd.path.values, ["{} (L= {})".format(d,L) for d,L in zip(dd.id_run.values, dd.length.values)],
                       [(s,e) for s,e in zip(dd.start.values, dd.end.values)],
                       os.path.join(output_path, "run_{:03}.png".format(run)),
                       "run {}".format(run))
        except Exception as ex:
            logger.exception("Mosaic picture failed: {}".format(ex))

    if not len(df):
        return None
    return df

if __name__ == "__main__":
    df = process_files(".")
    df.set_index(["id_run", "run", "length"]).to_csv("results.csv")
