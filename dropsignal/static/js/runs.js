// runs.js - Visualisation functions for the signal page.
// Copyright 2016 Guilhem Doulcier, Licence GNU AGPL3+
// This file is part of the dropsignal package.

// global Namespace
var ds_runs = {}

// Data loading using d3.queue.
d3_queue.queue()
	.defer(d3.csv, 'runs.csv', function(d) {
		return {
			// Field mapping between csv and javascript object.
			peaks: +d.peaks,
			id: +d.id
		}
	})
	.await(ready)

// Reduce functions
function init(){return 0}
function pk_add(p,v){p+= v.peaks; return p}
function pk_remove(p,v){p-= v.peaks; return p}


// Once the data is loaded...
function ready(error, runs) {
	if (error) { console.log(error) }
	//console.log('Runs loaded: ', runs)

	// Crossfilter init
	var runsFilter = crossfilter(runs)

	// Dimensions -- things to group or filter by.
	var dim_id = runsFilter.dimension(function(d) { return d.id })

	// Measures -- values to display.
	var measure_id =  dim_id.group().reduce(pk_add, pk_remove, init)

	//// Manage run visibility ////
	var runs_div = []
	// Collect the divs.
	for (var i=0; i<runs.length; i++){
		runs_div.push($('#run'+runs[i].id))
	}
	var updateRunVisibility = function(chart, filter){
		// This function update the runs div visibility
		var tp = $.map(dim_id.top(Infinity),function(d, j){return d.id})
		for(var i=0; i<runs_div.length; i++){
			if ($.inArray(i,tp)>-1){
				runs_div[i].show()
			}
			else{
				runs_div[i].hide()
			}
		}
	}
	////
	
	if (runs.length>1){
		// Charts
		ds_runs.run_selector  = dc.lineChart('#run_select')
		console.log('test')
		//$('#run_select')
		//	.before(' <a class="reset" href="javascript:ds_runs.run_selector.filterAll();dc.redrawAll();">reset selection</a><br/>')
		
		// Run selection chart configuration
		ds_runs.run_selector
			.width(1200).height(100)
			.dimension(dim_id)
			.group(measure_id)
			.x(d3.scale.linear().domain([0, runs.length-0.99]))
			.y(d3.scale.linear().domain([d3.min(runs, function(d) {return d.peaks})-1,
										 d3.max(runs, function(d) {return d.peaks})+1]))
			.xAxisLabel('Run index')
			.yAxisLabel('Droplets')
			.elasticY(true)
			.margins({top: 10, right: 30, bottom: 40, left: 70})
			.renderHorizontalGridLines(true)
			.renderVerticalGridLines(true)
			.renderArea(true)
		
		if (runs_div.length>1){
			ds_runs.run_selector
				.on('filtered', updateRunVisibility)
		} else {
			ds_runs.run_selector.brushOn(false)
		}
	}
	
	// Chart rendering
	dc.renderAll()
}
