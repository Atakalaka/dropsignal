// plate.js - An interactive microwell plate reprensentation.
// This file is part of the dropsignal package.
// Copyright 2018 Guilhem Doulcier, Licence GNU AGPL3+
// ---

function Plate(svg, width, height, droplets,well_to_color){
    // Extract row and columns names.
    var rows = d3.map(droplets, function(d){return d.well[0]}).keys().sort()
    var columns = d3.map(droplets, function(d){return d.well.substring(1)}).keys().map(function(d){return +d})
    columns.sort(function(a, b){return a-b})
    rows.sort()

    // Setup svg
    this.svg = svg
    this.svg.attr('width', width).attr('height', height)
    var r = Math.min(0.4*height/rows.length, 0.4*width/columns.length)

    // Set scales
    this.scale_col = d3.scaleBand().range([0,width]).domain(columns).padding(r)
    this.scale_row = d3.scaleBand().range([0,height]).domain(rows).padding(r)

    // Find proportion of selected entries per well.
    var nested_well = d3.nest()
	.key(function(d) { return d.well })
	.rollup(function(values) {
	    return d3.sum(values, function(d) { return d.selected })/values.length
	})
	.entries(droplets)

    var self = this
    // This might be a hack...
    // These functions have to be overridden with something useful see main_scatter.js
    this.stub_click_well = function (d){self.click_well(d)}
    this.stub_click_row = function (d){self.click_row(d)}
    this.stub_click_col = function (d){self.click_column(d)}



    // Add one circle per well.
    this.wells = this.svg.append('g').attr('class', 'well')

    this.wells.selectAll('circle')
	.data(nested_well)
	.enter().append('circle')
	.style('fill',function(d){return well_to_color[d.key]})
	.style('opacity',function(d){return d.value==0?'.3':d.value==1?'1':0.7})
	.attr('cy', function(d) { return self.scale_row(d.key[0])})
	.attr('cx', function(d) { return self.scale_col(d.key.substring(1))})
	.attr('r', r)
	.on('click',this.stub_click_well)

    // Add row and columns markers
    var tri = d3.symbol().type(d3.symbolTriangle)
    var row_symbols = this.svg.append('g')
	.attr('id','row_symbols')
	.selectAll('g')
	.data(rows).enter().append('g')
	.attr('transform', function(d) { return 'translate(' + 10 + ',' + self.scale_row(d) + ')'})
	.on('click',this.stub_click_row)

    row_symbols.append('path')
	.style('opacity','.10')
	.attr('transform',' rotate(90)')
	.attr('d', tri)
	.style('fill', 'black')
    row_symbols.append('text')
	.style('alignment-baseline','bottom')
	.style('text-anchor','middle')
	.text(function(d){return d})
	.style('opacity','.6')

    var col_symbols = this.svg.append('g')
	.attr('id','col_symbols')
	.selectAll('g')
	.data(columns).enter().append('g')
	.attr('transform', function(d) { return 'translate(' + self.scale_col(d) + ',' +15  + ')'})
	.on('click',this.stub_click_col)
    col_symbols.append('path')
	.attr('d', tri)
	.style('fill', 'black')
	.attr('transform',' rotate(180)')
	.style('opacity','.10')
    col_symbols.append('text')
	.attr('alignment-baseline','bottom')
	.style('text-anchor','middle')
	.text(function(d){return d})
	.style('opacity','.6')
}

Plate.prototype.draw = function(droplets){
    // Update color of the wells.
    var nested_well = d3.nest()
	.key(function(d) { return d.well })
	.rollup(function(values) {
	    return d3.sum(values, function(d) { return d.selected })/values.length
	})
	.entries(droplets)
    this.wells.selectAll('circle')
	.data(nested_well)
	.style('stroke',function(d){return d.value==1?'grey':null})
	.style('opacity',function(d){return d.value==0?'.3':d.value==1?'1':0.7})
}
