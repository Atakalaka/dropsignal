// protocol.js - Visualisation functions for the protocol page.
// This file is part of the dropsignal package.
// Copyright 2016 Guilhem Doulcier, Licence GNU AGPL3+

var disp_list = function(list){
    for(var i=0 ; i < list.length; i++){
	if ($.isArray(list[i])){
	    list[i] = list[i].join(', ')
	}
    }

    return list.join(', ')
}

var UrlExists = function(url)
    {
	var http = new XMLHttpRequest()
	http.open('HEAD', url, false)
	http.send()
	return http.status!=404
    }


// Parse the metadata file
$.getJSON('metadata.json', function( data ) {
    // A function to update the corresponding object field.
    var updater = function(){data[$(this).attr('id')] = $(this).val()}

    // Bind form fields to protocol fields.
    $('#comments').val(data.comments).change(updater)
    $('#title').val(data.title).change(updater)
    $('#experimentator').val(data.experimentator).change(updater)
    $('#machine').val(data.machine).change(updater)
    $('#strain').val(data.strain).change(updater)
    $('#expected_droplets').val(data.expected_droplets).change(updater)

    if (('droplets' in data) & (data.droplets.length!=0)){
	// If we have groups, add a table
	$('#droplets_groups').before('<h3>Groups</h3>')
	$('#droplets_groups').html('<thead><tr><th>Label</th><th>Droplets #</th><th>Expected Growth</th><th>Wells</th></tr></thead>')
	$('#droplets_groups').append('<tbody id="droplets_groups_body"></tbody>')
	var table = d3.select('#droplets_groups_body')
	var tr = table.selectAll('tr')
	    .data(data.droplets).enter()
	    .append('tr')
	tr.append('th').html(function(m) { return m.label })
	tr.append('td').append('div').attr('class', 'scroll').html(function(m) { return disp_list(m.id) })
	tr.append('td').html(function(m) { return m.expected_growth })
	tr.append('td').append('div').attr('class', 'scroll').html(function(m) { return disp_list(m.well)})

	// Add a download link for the template if it exists
	if (UrlExists('../template.csv')){
	    $( '#download_json' ).after('<li>Microwell Plate layout <a href="../template.csv">template.csv</a></li>')
	}
    }

    // Add a Downlad link.
    $( '#download_json' ).click(function() {
	this.href = 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(data,null,'\t'))
    })


})
