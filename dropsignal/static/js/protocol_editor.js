// protocol_editor.js - An interactive way of editing the protocol file.
// This file is part of the dropsignal package.
// Copyright 2018 Guilhem Doulcier, Licence GNU AGPL3+
// ---


$(window).ready(function(){
    var control = d3.select('#control')
    var advanced = false
    d3.selectAll('.advanced').style('display',advanced?'block':'none')
    d3.selectAll('.advanced_placeholder').style('display',!advanced?'block':'none')

    function toggle_advanced(){
	advanced = !advanced
	d3.selectAll('.show_advanced').text(advanced?'Hide Advanced settings':'Show Advanced settings')
	d3.selectAll('.advanced').style('display',advanced?'block':'none')
	d3.selectAll('.advanced_placeholder').style('display',!advanced?'block':'none')
    }
    function reset(){
	var form = d3.select('#protocol_form')
	form.select('#form_action').attr('value','reset')
	form.node().submit()
    }
    d3.select('#control_foot').append('span')
	.attr('class','btn btn-default show_advanced')
	.text('Show Advanced settings')
	.on('click', toggle_advanced)
    d3.select('#control_foot').append('span')
	.attr('class','btn btn-danger')
	.text('Reset to default')
	.on('click', reset)
    d3.selectAll('.advanced_placeholder')
	.append('span')
	.attr('class','btn btn-default show_advanced')
	.text('Show Advanced settings')
	.on('click', toggle_advanced)


    var raw_div = d3.select('#detect')
    var threshold_input = d3.select('#edit_detection_threshold input').on('change',request)
    var window_input = d3.select('#edit_window input').on('change',request)
    var window_size_input = d3.select('#edit_window_size input').on('change',request)
    var preview_on_runs = 3
    raw_div.append('h4').text('Detection preview')
    raw_div.attr('class','alert alert-info')

    var form = raw_div.append('form')
    form.append('label').text('Number of Runs: ')
    form.append('input')
	.attr('type','number')
	.attr('value',preview_on_runs)
	.on('change',function(){
	    preview_on_runs = this.value
	    request()
	})
    var out_div = raw_div.append('div')
    function request(){
	var thres = threshold_input.property('value')
	var window = window_input.property('value').replace(/['"]+/g, '')
	var window_size = window_size_input.property('value')
	var out = 'threshold:'+thres+' window: '+window+' window_size: '+window_size
	for (var run = 0; run<preview_on_runs; run++){
	    var uri = '/data/detect/?run='+run+'&threshold='+thres+'&window='+window+'&window_size='+window_size
	    console.log(uri)
	    d3.json(uri, function(error, json){
		out += '<div class="run">'
		out += '<strong>Run '+json.request.run+': </strong>'
		out += 'Detection 1: <span class="label label-primary">'+json.ptd1.number+' droplets </span>'
		out += 'Detection 2: <span class="label label-primary">'+json.ptd2.number+' droplets </span>'
		out += '</div>'
		out_div.html(out)
	    })
	}
}
})
