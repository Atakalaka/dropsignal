"""dropsignal -- A pipeline to process milidrop's machine output.
"""

import subprocess
import os
import sys

PYINSTALL = False
if getattr(sys, 'frozen', False):
    # If the application is run as a bundle, the pyInstaller bootloader
    # extends the sys module by a flag frozen=True and sets the app
    # path into variable _MEIPASS'.
    APP_PATH = os.path.join(sys._MEIPASS, 'dropsignal')
    PYINSTALL = True
else:
    APP_PATH = os.path.dirname(os.path.abspath(__file__))

def get_version():
    """Get the pipeline version number from git, the VERSION file, the
    COMMIT file or set it to unknown.
    """
    try:
        proc = subprocess.Popen("git describe --tags --dirty=.dev --always",
                                stdout=subprocess.PIPE, shell=True,
                                cwd=os.path.abspath(APP_PATH),
                                stderr=subprocess.DEVNULL)
        proc.wait(timeout=15)
        if proc.returncode != 0:
            raise Exception
        else:
            version = proc.communicate()[0].decode('utf8').strip()
            dev = '.dev' if 'dev' in version else ''
            version = '.'.join(version.split('-')[:-1])+dev
            open(os.path.join(APP_PATH, 'VERSION'),'w').write(version)
    except Exception:
        try:
            version = open(os.path.join(APP_PATH, 'VERSION'),'r').read()
        except Exception:
            try:
                version = open(os.path.join(APP_PATH, 'COMMIT'),'r').read()
                version = version.split(' ')[1][:10]
            except Exception:
                version = "unknown"
    return version

__version__ = get_version()

if __name__ == "__main__":
    print("This is dropsignal v{} ".format(__version__))
