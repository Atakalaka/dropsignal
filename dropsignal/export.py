"""export.py -- export functions"""
import glob
import os
import shutil
import logging
import json
import time
import numpy as np
import pandas as pd
import datetime

from dropsignal.plugins import INSTALLED
from dropsignal import __version__, APP_PATH
from dropsignal.log_formating import format_log

logger = logging.getLogger('dropsignal.export')


def header(static_path, scripts, title, active, inline_script=None, plugins=None, links=None, lib=['d3v3','jquery','bootstrap'], loader=True):
    """Return the html header

    Args:
       static_path (str): relative path to static folder
       scripts (list): name of the jsfiles to add.
       inline_script (str): inline javascritp to inject.
       title (str): Page <title>
       active (str): Which `name` of the navbar highlight as active.
       links (list of tuple): Pairs (`name`, `path`) of links in the navbar.
    """

    static_path = os.path.join(static_path, '') # Add a trailing / if it does not exists already
    static_path = static_path.replace("\\", "/") # Oh windows why don't you use normal slashes...
    script_html = ''

    if loader:
        lib+=['anime']

    for script in scripts:
        script_html += ('<script type="text/javascript"'
                        ' src="{static}js/{script}.js"> </script>\n').format(static=static_path,
                                                                             script=script)
    if inline_script is not None:
        script_html += '<script>'+inline_script+'</script>'
    lib_import = {
        'anime':'''
        <script type="text/javascript" src="{static}lib/js/anime.min.js"></script>
        ''',
        'd3v4':'''
        <script type="text/javascript" src="{static}lib/js/d3.v4.min.js"></script>
        ''',
        'd3v3':'''
        <script type="text/javascript" src="{static}lib/js/d3.v3.min.js"></script>
        <script type="text/javascript" src="{static}lib/js/d3-queue.v2.min.js"></script>
        ''',
        'bootstrap':'''
        <script type="text/javascript" src="{static}lib/js/bootstrap.min.js"></script>
        <link href="{static}lib/css/bootstrap.min.css" rel="stylesheet">
        ''',
        'crossfilter':'<script type="text/javascript" src="{static}lib/js/crossfilter.min.js"></script>',
        'dc':'''<script type="text/javascript" src="{static}lib/js/dc.min.js"></script>
        <link href="{static}lib/css/dc.css" type="text/css" rel="stylesheet">''',
        'jquery':'<script src="{static}lib/js/jquery.min.js"></script>'
    }
    lib_html = ''
    for li in lib:
        lib_html += lib_import[li].format(static=static_path)
    output = '''
    <!DOCTYPE html>
    <html lang="en">
    <head>

        <title>{title} - Dropsignal</title>
        <meta charset="utf-8">
        {lib}

        <link href="{static}css/main.css" rel="stylesheet" type="text/css">

        {scripts}
    </head>'''.format(static=static_path, title=title, scripts=script_html,lib=lib_html)

    output += '<body>'
    if loader:
        output += '''<div class='splash' id='wait'><svg width="300px", height="170px">
    <defs>
    <filter id="blurFilter4">
    <feGaussianBlur in="SourceGraphic" stdDeviation="1.5" />
    </filter>
</defs>
  <rect width="320" height="70" x="-10" y="65" id="tube" />
  <rect width="5" height="135" x="150" class="laser" />
  <rect width="20" height="20" x="142.5" />
  <circle cx="-40" cy="100px" r="30px" class="droplet"/>
  <circle cx="30" cy="100px" r="30px" class="droplet"/>
  <circle cx="100" cy="100px" r="30px" class="droplet" id="first"/>
  <circle cx="170" cy="100px" r="30px" class="droplet" />
  <circle cx="240" cy="100px" r="30px" class="droplet" />
  <circle cx="310" cy="100px" r="30px" class="droplet" />
</svg>
<script>var timeline = anime.timeline({loop:true})
var path = anime.path('#follow')
var droplets =  document.querySelectorAll('#wait .droplet')
timeline.add({
    targets: droplets,
    duration: 3000,
    easing:'linear',
    translateX: 70
})
    .add({
        targets: document.querySelectorAll('#wait #first'),
        duration: 2000,
        fill:  [
            {value: '#C24231'},
            {value: '#C29631'},
            {value: '#e7f5f8'}
        ],
        easing:'linear',
        offset: 1000
    })
    $( window ).on( 'load', function() {
        $('#wait').hide()
        })
</script>'''

    output += '''

    </div>
    <header class="navbar navbar-default">
    <div class="navbar-header">
    <a class="navbar-brand" href="/">Dropsignal</a>
    <ul class="nav navbar-nav">'''

    if links is None:
        links = [("Dynamics", "index.html"),
                 ("Scatter", "scatter.html"),
                 ("Signal", "signal.html"),
                 ("Log", "log.html")]
    for name, href in links:
        cls = 'class="active"' if active == name else ''
        output += ' <li {}><a href="{}">{}</a></li>'.format(cls, href, name)

    if plugins is not None:
        cls = 'active' if active == 'Plugins' else ''
        output += """
            <li class="dropdown {}">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Plugins <span class="caret"></span></a>
              <ul class="dropdown-menu">""".format(cls)
        for name in plugins:
            output += ' <li><a href="plugins.html?name={}">{}</a></li>'.format(name, str.capitalize(name.replace("_"," ")))
        output +="</ul></li>"

    output += '''
    </ul>
    </div>
    </header>'''
    return output

def footer():
    """ Return HTML footer"""
    return """
    <footer class="footer">
        <div class="container">
        <p class="text-muted">
            Automagically generated by <a href="https://gitlab.com/evomachine/dropsignal">dropsignal</a> v{} on {}. <a href="help.html">Get Help</a>
        </p>
    </div>
    </body>
    </html>'""".format(__version__, time.asctime())


def scatter_html(path,protocol):
    """Export an html file with an interactive scatter explorer"""
    output = header(static_path=os.path.join("static",""),
                    scripts=['uri_variables','scatter','selector','plate','legend','main_scatter'],
                    title="Scatter",
                    lib=['jquery','d3v4','bootstrap'],
                    plugins=protocol['enabled_plugins'],
                    active="Scatter")
    output += '''

  <main  class="container-fluid">
    <div class="row">
      <div id="conta_control"></div>
      <svg id="scatter"></svg>
    </div>

    <div class="row">
      <div id="plate" class="col-md-4">
        <h3> Microplate overview </h3>
      </div>
      <div id="legend" class="col-md-4">
        <h3> Groups </h3>
      </div>
      <div id="segments" class="col-md-4">
        <h3> Sorting selection </h3>
      </div>
    </div>
  </main>
    '''
    output+=footer()
    with open(os.path.join(path, 'scatter.html'), 'w') as fi:
        fi.write(output)

def rawsignal_html(path,protocol):
    """Export an html file with an interactive scatter explorer"""
    output = header(static_path=os.path.join("static",""),
                    scripts=['uri_variables','raw_explorer'],
                    title="Signal",
                    plugins=protocol['enabled_plugins'],
                    lib=['jquery','d3v4','bootstrap'],
                    active="Signal")
    output += '''
    <div id="hud"></div>
    <main  class="container-fluid">
    <div class="row">
    <svg> </svg>
    </div>
    </main>
    '''
    output+=footer()
    with open(os.path.join(path, 'signal.html'), 'w') as fi:
        fi.write(output)


def log_html(path,protocol):
    """Export an html file with the log"""
    output = header(scripts=[],
                    static_path=os.path.join("static",""),
                    title="Log",
                    plugins=protocol['enabled_plugins'],
                    lib=['jquery','bootstrap'],
                    active="Log")
    with open(os.path.join(path,'pipeline_log.txt')) as file:
        txt = '</br>'.join(map(format_log, file))
    output += '''
    <main  class="container-fluid">
    <h1>Analysis log</h1>
    {}
    </main>
    '''.format(txt)
    output+=footer()
    with open(os.path.join(path, 'log.html'), 'w') as fi:
        fi.write(output)






def runs_html(path, runs, protocol):
    """Export an html file listing the runs and their plots"""
    coalescence_events = protocol["coalescent_run_droplet"] if "coalescent_run_droplet" in protocol else {}

    output = header(static_path=os.path.join("static",""), scripts=['runs'], title="Signal", active="Signal", plugins=protocol['enabled_plugins'])
    output += '\n\n<div class="container"><div id="run_select"></div></div>'

    def timefmt(seconds):
        """convert time in human readable format"""
        if isinstance(seconds,datetime.timedelta):
            seconds = seconds.seconds
        minutes, seconds = divmod(seconds, 60)
        hours, minutes = divmod(minutes, 60)
        if not hours:
            if not minutes:
                out = '{} seconds'.format(int(seconds))
            else:
                out = '{}m {}s'.format(int(minutes), int(seconds))
        else:
            out = '{}h {}m {}s'.format(int(hours), int(minutes), int(seconds))
        return out

    files = sorted(glob.glob(os.path.join(path, 'peakdetect*.png')))
    files = dict([(int(os.path.basename(fi).split('.')[0][11:]), fi) for fi in files])

    for run in runs.index:
        if runs.loc[run, 'peaks'] == protocol["expected_droplets"]:
            label_type = 'success'
        elif runs.loc[run, 'good']:
            label_type = 'primary'
        else:
            label_type = 'danger'

        output += '''<div id="run{}"><aside><span class="label label-{}">Run {}</span>, {} , {} peaks,
        started at {}, lasted: {}
        '''.format(
            run,
            label_type,
            run,
            {1:'Forth', -1:"Back"}[runs.loc[run, 'direction']],
            runs.loc[run, 'peaks'],
            timefmt(runs.loc[run, 'time_start']),
            timefmt(runs.loc[run, 'duration']))
        labels = ['<span class="label label-danger">{}</span>'.format(x)
                  for x in runs.loc[run, 'tags']]
        output += '<span class="tags"> {} </span>'.format(" ".join(labels))
        if run in coalescence_events:
            output += 'Coalescence in {}'.format(coalescence_events[run])
        if run in files:
            output += '</aside><img src="{}"/></div>'.format(os.path.basename(files[run]))

    output += footer()
    with open(os.path.join(path, 'runs.html'), 'w') as fi:
        fi.write(output)

def plugins_html(path, protocol):
    """Export an html file listing the runs and their plots"""

    output = header(static_path=os.path.join("static",""), scripts=['plugins'], title="Plugins", active="Plugins", plugins=protocol['enabled_plugins'])
    output += ' <iframe id="frame" frameborder="0"></iframe><br/>'
    output += footer()
    with open(os.path.join(path, 'plugins.html'), 'w') as fi:
        fi.write(output)


def static(path, overwrite=True, back_link="../../", protocol=None):
    """Copy the packaged static files (css and javascript) into
    path/static"""
    if protocol is None:
        protocol = {'enabled_plugins':None}
    dest = os.path.join(path, 'static')
    if overwrite and os.path.exists(dest):
        try:
            shutil.rmtree(dest)
        except PermissionError as error:
            logger.error("{} {}".format(error,dest))
    static_path = os.path.join(APP_PATH, 'static')
    logger.debug("Static files are in {}".format(static_path))

    try:
        shutil.copytree(static_path, dest)
    except FileExistsError as error:
        logger.error("{} {}".format(error,dest))
    scripts = {'runs':['runs'],
               'index':['visualisation'],
               'protocol':["protocol"],
               'help':[]}
    for fi in glob.glob(os.path.join(dest, 'html', '*.html')):
        title = os.path.basename(fi).split('.')[0]
        static_rel_path = 'static/'
        name = {'runs':"Signal",
                "index": "Dynamics",
                "protocol":"Protocol",
                "help":"Help"}[title]

        try:

            with open(fi, 'r') as file:
                filedata = file.read()
                filedata = filedata.replace('{{header}}', header(static_path=static_rel_path,
                                                                 scripts=scripts[title],
                                                                 plugins=protocol['enabled_plugins'],
                                                                 title=name,
                                                                 active=name))
                filedata = filedata.replace('{{footer}}', footer())
            with open(fi, 'w') as file:
                file.write(filedata)
        except PermissionError as error:
            logger.error("{} {}".format(error,fi))

    logger.info("Copied static files (css/js) to {}".format(dest))
    return dest

def help_page(path, static_path):
    """Copy a fresh help page in the path"""
    help_sourcefile = os.path.join(os.path.join(APP_PATH, 'static'), 'html', 'help.html')
    try:
        shutil.copy(help_sourcefile, path)
    except PermissionError as error:
        logger.error("{} {}".format(error, help_sourcefile))

    fi = os.path.join(path, 'help.html')
    try:
        with open(fi, 'r') as file:
            filedata = file.read()
            filedata = filedata.replace('{{header}}', header(static_path=static_path,
                                                             scripts=[],
                                                             links=[("Index", "/"), ("Help", 'help.html')],
                                                             title="Help",
                                                             active="Help"))
            filedata = filedata.replace('{{footer}}', footer())
        with open(fi, 'w') as file:
            file.write(filedata)
    except PermissionError as error:
        logger.error("{} {}".format(error,fi))


def metadata(protocol, output_path):
    """Protocol export"""
    try:
        if protocol is not None:
            with open(os.path.join(output_path, 'metadata.json'), "w") as fi:
                json.dump(protocol, fi, skipkeys=True,
                          indent=True, sort_keys=True, default=str)
    except (TypeError, OSError) as ex:
        logger.error("Protocol export failed: {}".format(ex))
        logger.debug(protocol)

def analysis_folder(runs, droplets, droplet_dyn, aggregated,  protocol,  output_path):
    """ Populate the analysis folder """
    # Export Metadata
    try:
        metadata(protocol, output_path)
    except Exception as ex:
        logger.error("Metadata export failed")
        logger.error(ex)

    # Export the Signal page.
    try:
        runs_html(output_path, runs, protocol)
    except Exception as ex:
        logger.exception("Runs page export failed: {}".format(ex))

    rawsignal_html(output_path,protocol)
    log_html(output_path,protocol)
    plugins_html(output_path,protocol)

    # Export the scatter page
    try:
        scatter_html(output_path,protocol)
    except Exception as ex:
        logger.exception("Plot page scatter failed: {}".format(ex))

    # Export static files and extract the index.html page.
    try:
        static(output_path, protocol=protocol)
        shutil.copy(os.path.join(output_path, 'static', 'html', 'index.html'), output_path)
        shutil.copy(os.path.join(output_path, 'static', 'html', 'help.html'), output_path)
    except Exception as ex:
        logger.exception("Export of static files failed {}".format(ex))

    # Export dataframe as CSV and XLS.
    try:
        tables(runs, droplets, droplet_dyn,  aggregated, output_path, protocol['export_xls'],protocol['individual_csv'])
    except Exception as ex:
        logger.exception("Export of tables failed {}".format(ex))

def tables(runs, droplets, droplet_dyn, aggregated, output_path, export_xls=False, export_individual=True):
    """Export dataframe into csv files."""
    # CSV
    runs.to_csv(os.path.join(output_path, 'runs.csv'))
    def cfilter(x):
        suffix = x.split('_')[-1]
        return suffix != 'ptd1' and suffix != 'ptd2' and suffix != 'shift' and x != 'offset'
    col = [x for x in droplet_dyn.columns if cfilter(x)]
    droplet_dyn.loc[:, col].sort_values(['id_exp','run']).set_index(['id_exp','run']).to_csv(os.path.join(output_path, 'droplet_dynamics.csv'))
    aggregated.set_index(['kind','key']).to_csv(os.path.join(output_path, 'aggregated_dynamics.csv'))
    droplets.index.name = "droplet"
    droplets.to_csv(os.path.join(output_path, 'droplet.csv'))

    # Export a folder with a single CSV file per droplet containing all measures.
    if export_individual:
        droplet_path = os.path.join(output_path, 'droplets')
        if not os.path.exists(droplet_path):
            os.mkdir(droplet_path)
        for drop, table in droplet_dyn.groupby("id_exp"):
            table.set_index('run').to_csv(os.path.join(droplet_path, '{:04}.csv'.format(drop)))

    # XLSx export
    if export_xls:
        logger.debug('results.xlsx generation...')
        with pd.ExcelWriter(os.path.join(output_path, 'results.xlsx')) as xl:
            droplets.to_excel(xl, 'droplets')
            runs.set_index('id').to_excel(xl, 'runs')

            droplet_dyn.pivot('run', 'id_exp', 'time').to_excel(xl, 'time')
            values = [x for x in droplet_dyn.columns
                      if '_' in x and x.split("_")[-1] in frozenset(('mean', 'max', 'value'))]
            for val in values:
                pivoted = droplet_dyn.pivot('run', 'id_exp', val)
                pivoted.to_excel(xl, val)
                pivoted.to_csv(os.path.join(output_path, val+".csv"))
    logger.debug('done.')
