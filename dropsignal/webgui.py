"""webgui.py ~ web application serving the data
dropsignal - Guilhem Doulcier 2018 AGPL3+
"""
import logging
import threading
import os
import glob
import sys
import webbrowser
from hashlib import sha1
import concurrent.futures
import time
import socket
import argparse

import cherrypy
import cherrypy.process.plugins
import dropsignal.raw_explorer
import dropsignal.protocol_editor
from dropsignal.protocol import WEB_OPTIONS
import dropsignal.pipeline
import dropsignal.export
from dropsignal.log_formating import format_log
from dropsignal import __version__, APP_PATH

logger = logging.getLogger('dropsignal')
logger.setLevel(logging.DEBUG)
cherrypy.log.error_log.propagate = True
cherrypy.log.access_log.propagate = True

def exp_line(path, started, analysis, mount_point=None, **_):
    template = """<td>{path}</td> <td><form action="/edit/" method="post">
        <input type="hidden" name="path" value="{path}">
        <input class="btn-default btn" type="submit" value="Edit the protocol"></form></td>"""
    template += """<td><form action="/run/" method="post">
        <input type="hidden" name="path" value="{path}">
        <input class="btn-default btn" type="submit" value="Run the analysis"></form></td>"""
    if analysis:
        if not started:
            template += """<td><form action="/start_exp/" method="post">
            <input type="hidden" name="path" value="{path}">
            <input class="btn-primary btn" type="submit" value="Open the results"></form></td>"""
        else:
            template += """<td><form action="/stop_exp/" method="post">
            <a class="btn-success btn btn-group" href="{mount_point}">Go to results</a>
            <input type="hidden" name="path" value="{path}">
            <input class="btn-danger btn btn-group" type="submit" value="Close the results"></form></td>"""
    else:
        template += '<td>Run the analysis before</td>'
    return template.format(path=path, mount_point=mount_point)


class Dashboard(object):
    def __init__(self):
        self.app = []
        self.discovered = {}
        self.mount_set = set()
        self.runner = None
    def discover(self, path):
        files = glob.glob(os.path.join(path,'**','Raw0000.raw'))
        for f in files:
            key = os.path.dirname(f)
            if key not in self.discovered:
                self.discovered[key] = {'path':os.path.dirname(f),
                                        'started':False,
                                        'analysis':os.path.exists(os.path.join(os.path.dirname(f),'analysis'))
                }
            else:
                self.discovered[key]['analysis'] = os.path.exists(os.path.join(os.path.dirname(f),'analysis'))

    @cherrypy.expose
    def edit(self,path=None):
        if path is None:
            return 'You must provide a path'
        return dropsignal.protocol_editor.webui(self.discovered[path])

    @cherrypy.expose
    def save(self, action, **kargs):
        head = dropsignal.export.header('/static/',
                                        scripts=[],
                                        inline_script='''
                                        window.setTimeout(function() {{
                                        location.href = "/"
                                        }}, 1000)
                                        ''',
                                        title='Saving protocol',
                                        active='home',
                                        links=[('Home', '/'), ('Quit', '/stop')])
        if action == 'reset':
            try:
                pr = dropsignal.protocol_editor.save({'save_path':kargs['save_path']}, False)
                content = '''
                <main class='container'>
                <div class="alert success"><h1> Protocol cleared </h1>
                <pre>{}</pre>
                <a href="/">Return</a>.</div>
                </main>'''.format(pr)
            except Exception as ex:
                content = '''
                <main class='container'>
                <div class="alert danger"><h1> An error occured: {} </h1>
                <a href="/">Return</a>.</div>
                </main>'''.format(ex)
                raise ex
        else:
            try:
                pr = dropsignal.protocol_editor.save(kargs)
                content = '''
                <main class='container'>
                <div class="alert success"><h1> Protocol saved </h1>
                <pre>{}</pre>
                <a href="/">Return</a>.</div>
                </main>'''.format(pr)
            except Exception as ex:
                content = '''
                <main class='container'>
                <div class="alert danger"><h1> An error occured: {} </h1>
                <a href="/">Return</a>.</div>
                </main>'''.format(ex)
                raise ex
        return head + content + dropsignal.export.footer()

    @cherrypy.expose
    def run(self, path=None):
        if self.runner is not None:
            self.runner.stop()
            del self.runner
            self.runner = None
        self.runner = PipelineRunner(cherrypy.engine, self.discovered[path])
        self.runner.start()
        head = dropsignal.export.header('/static/',
                                        scripts=[],
                                        inline_script='''
                                        var http
                                        function update(){
                                        var wait = d3.select('#wait').attr('class','nosplash').style('display', 'block')
                                        d3.select('#anim').node().appendChild(wait.node())
                                        if (http.readyState == 4) {

                                        var response = http.responseText
                                        var logframe = document.getElementById('logframe')
                                        logframe.innerHTML = response

                                        $(logframe).animate({ scrollTop:  logframe.scrollHeight }, 1000);

                                        if (response.slice(-7)=='success'){
                                        d3.select('#done_div').style('display','block')
                                        window.setTimeout(function() {document.getElementById("done").submit()}, 2000)
                                        }

                                        setTimeout(refresh, 1000)
                                        }
                                        }

                                        function refresh() {
                                        http = new XMLHttpRequest()
                                        http.open('get', '/logger/')
                                        http.timeout = 1000
                                        http.onreadystatechange = update
                                        http.send(null)
                                        }

                                        refresh()
                                        ''',
                                        title='Running Dropsignal',
                                        active='home',
                                        links=[('Home', '/'), ('Quit', '/stop')])

        return head + '''
        <main class='container'>
        <div id="anim">        <h1> Running Dropsignal... </h1> </div><div id="logframe"> </div>
        <div id="done_div" style="display:none;" class='alert alert-success'>
        Done ! <form id="done" action="/start_exp/" method="post">
        <input type="hidden" name="path" value="{path}">
        <input class="btn btn-success" type="submit" value="Open the results"></form></div>
        </main>
        '''.format(path=path) + dropsignal.export.footer()
    @cherrypy.expose
    def logger(self):
        if self.runner is not None:
            try:
                txt = self.runner.logger()
            except Exception:
                raise cherrypy.HTTPError(404, "Error in runner")
        else:
            raise cherrypy.HTTPError(404, "No runner")
        return txt

    @cherrypy.expose
    def index(self, path=None):
        if path is None:
            path = os.getcwd()
        self.discover(path)

        head = dropsignal.export.header('static/',
                                        scripts=[],
                                        title='Dashboard',
                                        active='home',
                                        links=[('Home', '/'), ('Quit', '/stop')])

        form = """
        <form action="/" method="post">
        <label> Find more experiments in: </label>
        <input name="path" value={}>
        <input class="btn btn-default" type="submit" value="Scan"></form> """.format(path)

        if len(self.discovered):
            exp_list = """
            <h3> Experiments </h3>
            <table class='table'>
            <tr><th>Path</th><th>Edit</th><th>Run</th><th>Results</th></tr>
            <tr> {} </tr>
            </table>
            """.format("</tr>\n<tr>".join([exp_line(**v)
                                           for k,v
                                           in sorted(self.discovered.items())]))
        else:
            exp_list = 'No experiment found in {}'.format(path)
        return head + "<main class='container'>" + exp_list + form +"</main>" + dropsignal.export.footer()

    @cherrypy.expose
    def stop_exp(self, path=None):
        if path is None:
            return 'You must provide a path'
        self.discovered[path]['app'].teardown()
        del self.discovered[path]['app']
        self.discovered[path]['started'] = False
        self.mount_set.remove(self.discovered[path]['mount_point'])
        head = dropsignal.export.header('/static/',
                                        scripts=[],
                                        inline_script='''
                                        window.setTimeout(function() {{
                                        location.href = "/"
                                        }}, 1000)
                                        '''.format(self.discovered[path]['mount_point']),
                                        title='Running Dropsignal',
                                        active='home',
                                        links=[('Home', '/'), ('Quit', '/stop')])
        content = '''
            <main class='container'>
            <h1> Results closed. </h1>
            <a href="/">Return</a>.
            </main>'''
        return head + content + dropsignal.export.footer()

    @cherrypy.expose
    def start_exp(self, path=None):
        if path is None:
            return 'You must provide a path'
        try:
            # Start the app.
            self.discovered[path]['app'] = Experiment(os.path.expanduser(path))

            # Get the mounting point, taking in account that several
            # folder can have the same name.
            self.discovered[path]['mount_point'] = '/'+os.path.basename(path)
            i = 0
            while self.discovered[path]['mount_point'] in self.mount_set:
                self.discovered[path]['mount_point'] += str(i)
                i += 1
            self.mount_set.add(self.discovered[path]['mount_point'])

            # Mount the app.
            cherrypy.tree.mount(self.discovered[path]['app'],
                                self.discovered[path]['mount_point'],
                                self.discovered[path]['app'].conf)

        except Exception as ex:
            return 'Error encountered when starting the app: {}, path={}'.format(ex, path)
        else:
            self.discovered[path]['started'] = True
            head = dropsignal.export.header('/static/',
                                            scripts=[],
                                            inline_script='''
                                            window.setTimeout(function() {{
                                            location.href = "{0}/index.html"
                                            }}, 1000)
                                            '''.format(self.discovered[path]['mount_point']),
                                            title='Running Dropsignal',
                                            active='home',
                                            links=[('Home', '/'), ('Quit', '/stop')])
            content = '''
            <main class='container'>
            <h1> Loading... </h1>
            <a href="{0}/index.html">See the results</a>'''.format(self.discovered[path]['mount_point'])
            return head + content + '</main>'+ dropsignal.export.footer()
    @cherrypy.expose
    def stop(self):
        threading.Timer(1, lambda: os._exit(0)).start()
        return 'Dropsignal is closing. Goodbye !'

import mimetypes
mimetypes.types_map['.json'] = 'text/json'
mimetypes.types_map['.xlsx'] = 'application/x-download'


class PipelineRunner(cherrypy.process.plugins.SimplePlugin):
    """Run the pipeline"""
    def __init__(self, bus, experiment):
        cherrypy.process.plugins.SimplePlugin.__init__(self, bus)
        self.experiment = experiment
        self.executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)

        # clear the logfile
        self.logpath = os.path.join(self.experiment['path'],'analysis','pipeline_log.txt')
        if not os.path.exists(os.path.dirname(self.logpath)):
            os.mkdir(os.path.dirname(self.logpath))
        open(self.logpath, 'w').close()
        self.running = True

    def logger(self):
        content = ''
        end = False
        with open(self.logpath, 'r') as file:
            for line in file:
                content += '<p>'+format_log(line) + '</p>'
                if '~~~ END ~~~' in line:
                    content += '''success'''.format(path=self.experiment['path'])
                    end = True
            if not end  and not self.running:
                content +=  '''<div class='alert alert-danger'>
                <strong> Error: </strong> The analysis seems to have failed. <a href='/'> Return </a>. </div>
                '''
        return content
    def start(self):
        if self.running:
            self.executor.submit(dropsignal.pipeline.pipeline,
                                 self.experiment['path']).add_done_callback(self.stop)
    def stop(self,_=None):
        self.running = False
        try:
            self.executor.shutdown(wait=False)
        except RuntimeError:
            del self.executor
class Experiment(object):
    def __init__(self, path):
        self.path = path
        self.conf = {
            '/': {
                'tools.staticdir.on':True,
                'tools.staticdir.dir':os.path.join(os.path.abspath(self.path), "analysis"),
                'tools.staticdir.index':'index.html'
            },
            '/signal': {
                'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                'tools.response_headers.on': True,
                'tools.response_headers.headers': [('Content-Type', 'application/json')],
            },
            '/metadata': {
                'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                'tools.response_headers.on': True,
                'tools.response_headers.headers': [('Content-Type', 'application/json')],
            }
        }

        channel_prefix = sha1(str(self.path).encode('utf-8')).hexdigest()
        self.plugin = dropsignal.raw_explorer.RawDataPlugin(cherrypy.engine, path, channel_prefix)
        self.metadata = dropsignal.raw_explorer.MetadataGetter(channel_prefix)
        self.signal = dropsignal.raw_explorer.SignalGetter(channel_prefix)

        self.plugin.subscribe() #subscribe to start and stop channels.
        self.plugin.start() #Start the plugin.

    def teardown(self):
        self.plugin.stop()

def main():
    """Entry point for dropsignal_web"""
    print('Welcome in dropsignal  web ({}) !  '.format(__version__))
    parser = argparse.ArgumentParser(description='Dropsignal web user interface')
    parser.add_argument('--port', default=WEB_OPTIONS['web_initial_port'], help="Server port")
    parser.add_argument('--public', action='store_true',
                        default=WEB_OPTIONS['web_public'],
                        help="Serve on public IP")
    parser.add_argument('--browser',
                        default=WEB_OPTIONS['web_browser'],
                        action='store_true',
                        help="Open browser (dropsignal works best with firefox and will try it in priority)")
    parser.add_argument('--log', action='store_true', help="Webserver logging")
    parser.add_argument('path', metavar='PATH', type=str,
                        default=WEB_OPTIONS['web_default_path'], nargs='?',
                        help='Raw data location.')
    args = parser.parse_args()

    if args.path:
        os.chdir(os.path.expanduser(args.path))
    conf = {
        '/static':
        {
            'tools.staticdir.on':True,
            'tools.staticdir.dir':os.path.join(APP_PATH, 'static')
        },
        '/favicon.ico':
        {
            'tools.staticfile.on': True,
            'tools.staticfile.filename': os.path.join(APP_PATH, 'static', 'favicon.ico')
        }
    }

    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(logging.Formatter('%(levelname)-7s  %(message)s'))
    logger.addHandler(ch)

    cherrypy.tree.mount(Dashboard(), '/', conf)
    start_server(port=int(args.port),
                 open_webbrowser=args.browser,
                 public=args.public,
                 log=args.log)

def start_server(port=None, open_webbrowser=False, public=False, log=False):
    port = int(port)
    iport = port


    ## Try to find an open port.
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    def check_port(port):
        try:
            s.bind(("127.0.0.1", port))
        except OSError as e:
            # On windows, a OSError "winerror 10048" is thrown if the
            # port is in use.
            logger.info("Port {} is already in use: {}".format(port, e))
            return False
        except socket.error as e:
            if e.errno == 98:
                logger.info("Port {} is already in use".format(port))
                return False
            else:
                raise e
        else:
            return True
    while not check_port(port) and port < iport+10:
        port += 1
    s.close()
    if port >= iport+10:
        logger.critical('Failed to find an open port.')
        return False

    ## Configure and start cherrypy
    cherrypy.config.update({
        'server.socket_port': int(port),
        'server.socket_host':'0.0.0.0' if public else '127.0.0.1',
        'log.screen':log,
        'tools.staticdir.debug': log,})

    # webbrowser.
    if open_webbrowser:
        try:
            browser = webbrowser.get('firefox')
        except webbrowser.Error:
            if os.path.exists(WEB_OPTIONS['web_firefox_path']):
                # Ugly hack for windows.
                import subprocess
                class Br(object):
                    def open(self, url):
                        logger.debug('Calling Firefox from {}'.format(WEB_OPTIONS["web_firefox_path"]))
                        subprocess.call('"{}" {}'.format(WEB_OPTIONS["web_firefox_path"], url))
                browser = Br()
            else:
                logger.warning(('Unable to find Firefox, will use default browser instead. '
                                'Dropsignal works best with Mozilla Firefox !'))
                browser = webbrowser.get(None)
        cherrypy.engine.start_with_callback(browser.open,
                                            ('http://localhost:{}'.format(port),))
    else:
        cherrypy.engine.start()
    logger.info("Serving {} on http://{}:{}".format('publicly' if public else '',
                                                    '127.0.0.1' if not public else '0.0.0.0',
                                                    port))
    return port

if __name__ == '__main__':
    main()
