dropsignal\.plugins\.curve\_fitting package
===========================================

Module contents
---------------

.. automodule:: dropsignal.plugins.curve_fitting
    :members:
    :undoc-members:
    :show-inheritance:
